# Modul 3

### Kelompok D09
| Nama | NRP |
| ------ | ------ |
| Christian Kevin Emor | 5025211153 |
| Andrian Tambunan | 5025211018 |
| Helmi Abiyu Mahendra | 5025211061 |

### Problem 1
##### Deskripsi Problem:
Lord Maharaja Baginda El Capitano Harry Maguire, S.Or., S.Kom yang dulunya seorang pemain bola, sekarang sudah pensiun dini karena sering blunder dan merugikan timnya. Ia memutuskan berhenti dan beralih profesi menjadi seorang programmer. Layaknya bola di permainan sepak bola yang perlu dikelola, pada dunia programming pun perlu adanya pengelolaan memori. Maguire berpikir bahwa semakin kecil file akan semakin mudah untuk dikelola dan transfer file-nya juga semakin cepat dan mudah. Dia lantas menemukan Algoritma Huffman untuk proses kompresi lossless. Dengan kepintaran yang pas-pasan dan berbekal modul Sisop, dia berpikir membuat program untuk mengkompres sebuah file. Namun, dia tidak mampu mengerjakannya sendirian, maka bantulah Maguire membuat program tersebut! 
(Wajib menerapkan konsep pipes dan fork seperti yang dijelaskan di modul Sisop. Gunakan 2 pipes dengan diagram seperti di modul 3).

#### Point 1A
##### Deskripsi Problem:
Pada parent process, baca file yang akan dikompresi dan hitung frekuensi kemunculan huruf pada file tersebut. Kirim hasil perhitungan frekuensi tiap huruf ke child process.
##### Solusi
bagian dari parent process akan membaca file dan menghitung frekuensi kemunculan huruf pada file tersebut, serta mengirim hasil perhitungan frekuensi tiap huruf ke child process.

##### Potongan Code
```
void count_freq(const char* filename, unsigned int freq[MAX_CHAR]) {

    // buka file dengan mode r (read only) dan simpan file ke pointer bernama FILE
    FILE *file = fopen(filename, "r");
    //kondisi apakah file berhasil dibuka atau tidak, jika tidak berhasil maka program akan keluar 
    if (file == NULL) {
        exit(1);
    }

    //variabel c dengan tipe data integer
    int c;
    // membaca karakter satu persatu dari file yang dibuka sebelumnya hingga akhir file (EOF) tercapai
    while ((c = fgetc(file)) != EOF) {
        // menambah jumlah frekuensi karakter pada array freq sesuai dengan karakter yang dibaca
        freq[c]++;
    }

    //menutup file yang telah dibuka sebelumnya dengan funsi fclose
    fclose(file);
}

```
##### Penjelasan
`count_freq()`. Fungsi ini membuka file dengan mode "r" (read-only) dan membaca karakter satu per satu dari file tersebut hingga EOF (end-of-file) tercapai. Setiap karakter yang dibaca akan menambah jumlah frekuensi kemunculan karakter pada array `freq`. Array `freq` digunakan untuk menyimpan frekuensi kemunculan tiap karakter dalam file yang akan dikompresi dan akan dikirimkan ke child process untuk dihitung lebih lanjut.

#### Point 1B
##### Deskripsi Problem:
Pada child process, lakukan kompresi file dengan menggunakan algoritma Huffman berdasarkan jumlah frekuensi setiap huruf yang telah diterima.
##### Solusi
Pada child process, terima data frekuensi setiap huruf yang dikirimkan oleh parent process, Buat pohon Huffman berdasarkan data frekuensi huruf yang diterima. Lakukan kompresi pada file dengan menggunakan pohon Huffman yang telah dibuat. Simpan file yang telah terkompresi.
##### Code
```
// fungsi untuk menulis data ke dalam file compressed
void write_code(const char* filename, char* huffCode[MAX_CHAR]) {
    // buka file dengan mode r (read only) dan simpan file ke pointer bernama inputFile
    FILE *inputFile = fopen(filename, "r");
    //kondisi apakah file berhasil dibuka atau tidak, jika tidak berhasil maka program akan keluar 
    if (inputFile == NULL) {
        exit(1);
    }

    // buka file dengan mode w (write only) dan simpan file ke pointer bernama outputFile
    FILE *outputFile = fopen("compressed.txt", "w");
    //kondisi apakah file berhasil dibuka atau tidak, jika tidak berhasil maka program akan keluar 
    if (outputFile == NULL) {
        exit(1);
    }

    // variabel c dengan tipe data integer
    int c;
    // loop membaca karakter satu persatu dari file yang dibuka sebelumnya hingga akhir file (EOF) tercapai
    while ((c = fgetc(inputFile)) != EOF) {
        // tulis kode huffman untuk karakter yang dibaca ke dalam file compressed
        fputs(huffCode[c], outputFile);
    }

    //menutup file yang telah dibuka sebelumnya dengan funsi fclose
    fclose(inputFile);
    fclose(outputFile);
}


```
##### Penjelasan
Potongan codingan di atas adalah fungsi untuk menulis data ke dalam file compressed dengan menggunakan algoritma Huffman. Fungsi ini membuka file dengan mode "read only" dan kemudian membaca karakter satu persatu dari file tersebut hingga akhir file tercapai. Selanjutnya, fungsi akan menuliskan kode Huffman untuk karakter yang dibaca ke dalam file compressed. Fungsi ini juga akan menutup file yang telah dibuka sebelumnya dengan menggunakan fungsi fclose.

#### Point 1C
##### Deskripsi Problem:
Kemudian (pada child process), simpan Huffman tree pada file terkompresi. Untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman dan kirimkan kode tersebut ke program dekompresi menggunakan pipe.
##### Solusi
kita akan meyimpan huffman tree dan membuatnya menjadi kode huffman dan kirim menggunakan pipe

##### Code
```
// pada child process, simpan Huffman tree pada file terkompresi
// untuk setiap huruf pada file, ubah karakter tersebut menjadi kode Huffman
// dan kirimkan kode tersebut ke program dekompresi menggunakan pipe

if (pid == 0) { //child process
    //membuat file dengan nama file compressed
    int fd = open("compressed", O_WRONLY | O_CREAT | O_TRUNC, 0644);
    // menuliskan frekuensi karakter pada file compressed
    write(fd, freq, MAX_CHAR * sizeof(unsigned int));

    //membuat huffman tree
    struct Node *root = create_huffman(freq);
    //menginisialisasi array huffCode dengan karakter null
    char* huffCode[MAX_CHAR];
    for (int i = 0; i < MAX_CHAR; i++) {
        huffCode[i] = NULL;
    }

    //menggenerate kode huffman
    char code[MAX_CHAR];
    code[0] = '\0';
    generate_code(root, code, 0, huffCode);

    //proses membaca file dan mengkompresi teks
    char buffer[BLOCK];
    int nread;
    while ((nread = read(fd_input, buffer, BLOCK)) > 0) {
        for (int i = 0; i < nread; i++) {
            //menuliskan kode huffman untuk karakter ke file compressed
            char* code = huffCode[buffer[i]];
            int len = strlen(code);
            for (int j = 0; j < len; j++) {
                write(fd, &code[j], 1);
            }
        }
    }
    //menutup file compressed
    close(fd);
    exit(0);
}
```
##### Penjelasan
Pada potongan kode di atas, kita dapat melihat bagaimana di child process, Huffman tree dibuat dan disimpan pada file terkompresi. Setelah itu, untuk setiap karakter pada file yang akan dikompresi, karakter tersebut diubah menjadi kode Huffman dan dikirimkan ke program dekompresi menggunakan pipe.

#### Point 1D
##### Deskripsi Problem:
Kirim hasil kompresi ke parent process. Lalu, di parent process baca Huffman tree dari file terkompresi. Baca kode Huffman dan lakukan dekompresi. 
##### Solusi
Baca huffman tree lalu kita akan melakukan dekompresi
##### Code
```
int pipe_fd[2];

// membuat pipe
if (pipe(pipefd) == -1) {
    perror("pipe");
    exit(1);
}

// fork process
pid_t pid = fork();

// jika pid == 0, maka ini adalah child process
if (pid == 0) {
    // tutup bagian tulis dari pipe
    close(pipefd[1]);

    // redirect input dari pipe
    dup2(pipefd[0], STDIN_FILENO);

    // jalankan program dekompresi
    char *args[] = {"./decompress", NULL};
    execvp(args[0], args);

    // keluar jika gagal
    perror("execvp");
    exit(1);
} else {
    // tutup bagian baca dari pipe
    close(pipefd[0]);

    // tulis hasil kompresi ke pipe
    write(pipefd[1], compressed_data, compressed_size);

    // tutup bagian tulis dari pipe
    close(pipefd[1]);

    // tunggu child process selesai
    wait(NULL);

    // baca Huffman tree dari file terkompresi
    FILE *in = fopen(argv[1], "rb");
    struct Node *root = read_tree(in);

    // baca kode Huffman dari pipe
    char *huffman_code[MAX_CHAR];
    read(pipefd[0], huffman_code, sizeof(huffman_code));

    // lakukan dekompresi
    decompress(huffman_code, compressed_size, root);
}
```
##### Penjelasan
kode di atas melakukan pembuatan pipe untuk mengirimkan hasil kompresi dari child process ke parent process. Setelah itu, hasil kompresi ditulis ke bagian tulis dari pipe. Di parent process, Huffman tree dibaca dari file terkompresi menggunakan fungsi `read_tree()` dan hasilnya disimpan di variabel `root`. Selanjutnya, kode Huffman dibaca dari bagian baca dari pipe dan disimpan di array `huffman_code`. Terakhir, dekompresi dilakukan menggunakan fungsi `decompress()`.


#### Point 1E
##### Deskripsi Problem:
Di parent process, hitung jumlah bit setelah dilakukan kompresi menggunakan algoritma Huffman dan tampilkan pada layar perbandingan jumlah bit antara setelah dan sebelum dikompres dengan algoritma Huffman.
##### Solusi
Hitung jumlah bit lalu tampilkan perbandingan setelah dan sebelum dikompres
##### Code
```
// proses parent
if (pid > 0) {
    // tunggu proses child selesai
    wait(NULL);
    // hitung ukuran file sebelum dikompresi
    struct stat st;
    stat(argv[1], &st);
    double size_before = st.st_size * 8;
    // hitung ukuran file setelah dikompresi
    double size_after = count_bits(huffCode, freq) * 1.0;
    // tampilkan perbandingan ukuran file sebelum dan sesudah dikompresi
    printf("Ukuran file sebelum kompresi: %.2lf bit\n", size_before);
    printf("Ukuran file setelah kompresi: %.2lf bit\n", size_after);
    printf("Rasio kompresi: %.2lf%%\n", (size_before - size_after) / size_before * 100);
}
```
##### Penjelasan
program menunggu proses child selesai dengan menggunakan `wait(NULL)` untuk memastikan bahwa proses child telah selesai sebelum dilanjutkan. Setelah itu, program menghitung ukuran file sebelum dan setelah dikompresi dengan menggunakan fungsi `count_bits()` yang menghitung jumlah bit setelah kompresi dengan algoritma Huffman. Kemudian, program menampilkan perbandingan ukuran file sebelum dan setelah dikompresi, serta rasio kompresi dalam persen.

### Problem 2
##### Deskripsi Problem:
Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.

#### Point 2A
##### Deskripsi Problem:
Membuat program C dengan nama `kalian.c`, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah 4×2 dan matriks kedua 2×5. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah 1-5 (inklusif), dan rentang pada matriks kedua adalah 1-4 (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

##### Solusi:
Men-generate angka random  dan ukuran matrix sesuai yang dibutuhkan. kemudian melakukan perkalian.

##### Potongan Kode:
```
printf("Generate Matriks 1:\n");
   for(unsigned long long i = 0; i < 4; i++)
   {
       for(unsigned long long j = 0; j < 2; j++)
       {
           matrix1[i][j] = (rand() % 5) + 1;
           /*
           menggunakan fungsi random yang akan meng-generate angka acak
           yang mana angka random tersebut akan di mod 5 +1, untuk mengambil angka
           yang HANYA diantara 1 hingga 5
           */
           printf("%lld ", matrix1[i][j]);
           /*
           Untuk menampilkan nilai elemen matriks pada baris ke-i dan kolom ke-j,
           yang disimpan dalam variabel matrix1. %lld digunakan sebagai specifier untuk tipe data long long.
           */
       }
       printf("\n"); // spasi
   }
   printf("\nGenerate Matriks 2:\n");
   for(unsigned long long i = 0; i < 2; i++)
    // menggunakan unsigned long long untuk menyamakan variabel generate angka random matrix.
    // dinyatakan i(baris) hanya boleh sampai 2, maka loop 2x
   {
       for(unsigned long long j = 0; j < 5; j++)
        // dinyatakan j(kolom) hanya boleh sampai 5, maka loop 5x
       {
           matrix2[i][j] = (rand() % 4) + 1;
           /*
           menggunakan fungsi random yang akan meng-generate angka acak
           yang mana angka random tersebut akan di mod 4 +1, untuk mengambil angka
           yang HANYA diantara 1 hingga 4
           */
           printf("%lld ", matrix2[i][j]);
           /*
           Untuk menampilkan nilai elemen matriks pada baris ke-i dan kolom ke-j,
           yang disimpan dalam variabel matrix2. %lld digunakan sebagai specifier untuk tipe data long long.
           */
       }
       printf("\n"); // spasi
   }
   for(unsigned long long i = 0; i < 4; i++)
    // untuk mengalikan.
    // menggunakan unsigned long long untuk menyamakan variabel generate angka random matrix.
    // loop 4x untuk baris dikarenakan perkalian matrix.
   {
       for(unsigned long long j = 0; j < 5; j++)
        // menggunakan unsigned long long untuk menyamakan variabel generate angka random matrix.
        // loop 4x untuk baris dikarenakan perkalian matrix.
       {
           hasil[i * 5 + j] = 0;
           for(unsigned long long x = 0; x < 2; x++)// pengalian matrix.
           {
               hasil[i * 5 + j] += matrix1[i][x] * matrix2[x][j];
               // menyimpan hasil perkalian matrix di variabel hasil.
           }
       }
   }
   printf("\nHasil Perkalian Matriks:\n");
   for(unsigned long long i = 0; i < 4; i++)
    // menggunakan unsigned long long untuk menyamakan variabel generate angka random matrix.
    // loop 4x karena hasil perkalian matrix ini adalah 4 baris.
   {
       for(unsigned long long j = 0; j < 5; j++)
        // menggunakan unsigned long long untuk menyamakan variabel generate angka random matrix.
        // loop 5x karena hasil perkalian matrix ini adalah 5 kolom.
       {
           printf("%lld ", hasil[i * 5 + j]); // print hasil matrix.
       }
       printf("\n"); // spasi
   }
   shmdt(hasil);
   
```
##### Penjelasan
1. `rand()` : Untuk meng-generate angka acak.
2. `shmdt()` : Untuk untuk melepaskan/merelase alamat memori yang telah dialokasikan
untuk segment shared memory.
3. `*result` = Sebagai pointer yang menunjuk ke lokasi memori yang sama dengan shared memory yang digunakan.
4. `shmid` = Untuk menyimpan id shared memory yang digunakan.
5. `srand()` : Untuk mengambil parameter waktu saat itu .

#### Point 2B
##### Deskripsi Problem:
Buatlah program C kedua dengan nama `cinta.c`. Program ini akan mengambil variabel hasil perkalian matriks dari program `kalian.c` (program sebelumnya). Tampilkan hasil matriks tersebut ke layar. 
(Catatan: wajib menerapkan konsep shared memory)

##### Solusi:
Mengkalkulasikan matrix menggunakan shared memory, dan mencatat waktunya.

##### Potongan Kode:
```
void *calculate_factorials(void *arg)
/*
Digunakan sebagai fungsi thread.
Fungsi ini menerima argumen berupa pointer ke array barisKolom
yang berisi indeks baris dan kolom dari elemen matriks yang a
kan dihitung faktorialnya.
*/
{
  int *barisKolom = (int*)arg;
  // Pointer arg dan menyimpannya ke dalam variabel i dan j.
  unsigned long long i = barisKolom[0];
  unsigned long long j = barisKolom[1];
  /*
  Program kemudian membuat 20 thread yang akan menjalankan fungsi
  factorial_thread dengan masing-masing thread bertanggung jawab
  menghitung faktorial dari satu elemen matriks.
  Indeks baris dan kolom dari setiap elemen matriks
  dikirim sebagai argumen ke fungsi factorial_thread.
  */
  unsigned long long hasil = factorial(temp[i * 5 + j]); // pamggil fungsi factorial
  // panggil fungsi factorial
  temp[i * 5 + j] = hasil;
  pthread_exit(NULL);
  // fungsi akan memanggil pthread_exit() untuk keluar dari thread
}
int main()
{
  clock_t start = clock();
  // untuk memulai perhitungan waktu, yang disimpan kesalam variabel start.
  unsigned long long shmid, *hasil;
   /*
  menggunakan unsigned long long untuk menyamakan variabel generate angka random matrix.
  *hasil = digunakan sebagai pointer yang menunjuk ke lokasi memori yang sama dengan shared memory yang digunakan.
  shmid = untuk menyimpan id shared memory yang digunakan.
  */
  key_t key = 1234; // untuk mengidentifikasi shared memory.
  shmid = shmget(key, sizeof(unsigned long long[4][5]), 0666);
  /*
  program membuat shared memory dengan menggunakan fungsi shmget().
  Fungsi ini membutuhkan tiga parameter, yaitu key, ukuran shared memory dalam byte, dan flag
  yang digunakan untuk mengontrol hak akses shared memory.


  Disini ukuran shared memory yang dibuat sesuai dengan ukuran matriks hasil perkalian, yaitu 4x5.
  Flag yang digunakan adalah 0666,
  yang mengindikasikan untuk membuat shared memory.
  */
  hasil = shmat(shmid, NULL, 0); // untuk meng-attach shared memory ke variabel hasil.


  printf("Berikut adalah Matriks Awal di dalam file 'Kalian':\n");
  for(unsigned long long i = 0; i < 4; i++)
  // digunakan untuk mengiterasi baris matriks..
  {
      for(unsigned long long j = 0; j < 5; j++)
      // digunakan untuk mengiterasi kolom matriks.
      {
          printf("%lld ", hasil[i * 5 + j]);
          // digunakan untuk menampilkan elemen matriks pada baris i dan kolom j.
      }
      printf("\n");
  }


  temp = (unsigned long long *) malloc(sizeof(unsigned long long[4][5]));
  // malloc = mengalokasikan memori dinamin ukuran 4 x 5.
  if (temp == NULL) { // cek apalah alokasi memori berhasil atau tidak.
  // kalau temp = null, keluar.
      perror("malloc");
      exit(1); // keluar
  }
  for(unsigned long long i = 0; i < 4; i++)
  // digunakan untuk mengiterasi baris matriks.
  {
      for(unsigned long long j = 0; j < 5; j++)
      // digunakan untuk mengiterasi kolom matriks.
      {
          temp[i * 5 + j] = hasil[i * 5 + j]; // memberikan nilai hasil ke temp.
          // seluruh hasil dari variabel hasil dicopy ke variabel temp.
      }
  }
  pthread_t threads[20]; // deklarasikan array thread
  // ukuran 20
  int index[20][2];
  // deklarasi index yang berisi indeks baris dan kolom elemen matriks.
  int count = 0; // hitung jumlah thread.


  printf("\nBerikut adalah hasil faktorial dari Matrix awal:\n");
  for(unsigned long long i = 0; i < 4; i++)
  // digunakan untuk mengiterasi baris matriks
  // yang mana matriks memiliki 4 baris
  {
      for(unsigned long long j = 0; j < 5; j++)
      // digunakan untuk mengiterasi kolom matriks.
      // yang mamna matriks memiliki 5 kolom.
      {
          index[count][0] = i;
          // Menetapkan nilai indeks baris i dari matriks ke indeks
          // pertama dari array 2 dimensi 'index' dengan indeks ke-'count'.
          index[count][1] = j;
          // Menetapkan nilai indeks kolom j dari matriks ke indeks
          // kedua dari array 2 dimensi 'index' dengan indeks ke-'count'.
          pthread_create(&threads[count], NULL, calculate_factorials, (void *)&index[count]);
          /*
          Membuat thread baru dan menetapkan fungsinya ke 'calculate_factorials'.
          Menetapkan argumen untuk fungsi tersebut dengan alamat dari array 2
          dimensi 'index' pada indeks ke-'count'.
          Menyimpan ID thread baru ke dalam array.
          */
          count++; // tambah 1 hitungan thread
      }
  }
  for(int i = 0; i < count; i++)
  // Mengulang iterasi untuk setiap thread yang dibuat, mulai dari 0 hingga 'count' - 1.
  {
      pthread_join(threads[i], NULL);// Menunggu thread sesuai selesai dieksekusi.
      // Menunggu thread dengan ID yang sesuai selesai dieksekusi.
  }


  for(unsigned long long i = 0; i < 4; i++)
  // Mengulang iterasi untuk indeks baris i dalam matriks, mulai dari 0 hingga 3.
  // yang mana matriks memiliki 4 baris
  {
      for(unsigned long long j = 0; j < 5; j++)
      // Mengulang iterasi untuk indeks kolom j dalam matriks, mulai dari 0 hingga 4.
      // yang mamna matriks memiliki 5 kolom.
      {
          printf("%lld ", temp[i * 5 + j]);
          // Menampilkan nilai elemen matriks pada baris i dan kolom j
          // dengan memperoleh nilai dari array 'temp' pada indeks yang sesuai.
      }
      printf("\n"); // spasi
  }
  shmdt(hasil); // Mengeksekusi operasi shmdt pada shared memory 'hasil',
```
##### Penjelasan
1. `*hasil` : Digunakan sebagai pointer yang menunjuk ke lokasi memori yang sama dengan shared memory yang digunakan.
2. `shmdt()` : Untuk untuk melepaskan/merelase alamat memori yang telah dialokasikan
untuk segment shared memory.
3. `*result` = Sebagai pointer yang menunjuk ke lokasi memori yang sama dengan shared memory yang digunakan.
4. `shmid` = Untuk menyimpan id shared memory yang digunakan.
5. `srand()` : Untuk mengambil parameter waktu saat itu .

#### Point 2C
##### Deskripsi Problem:
Setelah ditampilkan, berikutnya untuk setiap angka dari matriks tersebut, carilah nilai faktorialnya. Tampilkan hasilnya ke layar dengan format seperti matriks.

##### Potongan Kode:
```
for(unsigned long long i = 0; i < 4; i++)
  // Mengulang iterasi untuk indeks baris i dalam matriks, mulai dari 0 hingga 3.
  // yang mana matriks memiliki 4 baris
  {
      for(unsigned long long j = 0; j < 5; j++)
      // Mengulang iterasi untuk indeks kolom j dalam matriks, mulai dari 0 hingga 4.
      // yang mamna matriks memiliki 5 kolom.
      {
          printf("%lld ", temp[i * 5 + j]);
          // Menampilkan nilai elemen matriks pada baris i dan kolom j
          // dengan memperoleh nilai dari array 'temp' pada indeks yang sesuai.
      }
      printf("\n"); // spasi
  }
```
##### Penjelasan
1. Mengulang iterasi untuk indeks baris i dalam matriks, mulai dari 0 hingga 3.

#### Point 2D
##### Deskripsi Problem:
Buatlah program C ketiga dengan nama `sisop.c`. Pada program ini, lakukan apa yang telah kamu lakukan seperti pada `cinta.c` namun tanpa menggunakan thread dan multithreading. Buat dan modifikasi sedemikian rupa sehingga kamu bisa menunjukkan perbedaan atau perbandingan (hasil dan performa) antara program dengan multithread dengan yang tidak. 
Dokumentasikan dan sampaikan saat demo dan laporan resmi.

##### Solusi:
Untuk menunjukkan perbandingan performa, ditentukan dengan menggirung waktu file berjalan.

##### Potongan Kode:
```
printf("\nWaktu yang dibutuhkan adalah %lf detik.\n", (double) (end - start) /CLOCKS_PER_SEC);

```

### Problem 3
##### Deskripsi Problem:
Elshe saat ini ingin membangun usaha sistem untuk melakukan stream lagu. Namun,
Elshe tidak paham harus mulai dari mana.
#### Point 3a
##### Deskripsi Problem:
Bantulah Elshe untuk membuat sistem stream (receiver) stream.c dengan
user (multiple sender dengan identifier) user.c menggunakan message
queue (wajib). Dalam hal ini, user hanya dapat mengirimkan perintah berupa
STRING ke sistem dan semua aktivitas sesuai perintah akan dikerjakan
oleh sistem.
##### Solusi
##### Potongan Code
```
```
#### Point 3b
##### Deskripsi Problem:
User pertama kali akan mengirimkan perintah DECRYPT kemudian sistem
stream akan melakukan decrypt/decode/konversi pada file song-playlist.json sesuai metodenya dan meng-output-kannya menjadi playlist.txt diurutkan menurut alfabet.
##### Solusi
Melakukan decrypt dengan 3 metode yang telah ditentukan, yaitu ROT13, base34, dan hex.

ROT13
Enkripsi pada tiap karakter di string dengan menggesernya
sebanyak 13 karakter.
Base64
Encoding dari data biner menjadi teks
Hex
Penggunaan kombinasi bilangan hexadesimal (0-9
dan A-F) yang merepresentasikan suatu string.
##### Code
Decrypt
```sh
void decryptLoadJSON() {
  FILE *fp = fopen("song_playlist.json", "rb");
  // buka file playlist
  char buffer[1024];

  if(fp == NULL) {
    printf("Error opening file\n");
    exit(1);
  }

  struct json_object *parsed_json;
  struct json_object *method;
  struct json_object *title;

  fread(buffer, 1024, 1, fp);
  fclose(fp);

  parsed_json = json_tokener_parse(buffer);

  json_object_object_get_ex(parsed_json, "method", &method);
  json_object_object_get_ex(parsed_json, "title", &title);

  const char *method_str = json_object_get_string(method);
  const char *title_str = json_object_get_string(title);

// pemisahan metode decrypt berdasarkan ketentuan dalam file
  if(strcmp(method_str, "rot13") == 0) {
    rot13(title_str);
  }
  else if(strcmp(method_str, "base64Text") == 0) {
    base64TextDecr(title_str);
  }
  else if(strcmp(method_str, "hex") == 0) {
    hexDecr(title_str);
  }

  FILE *playlist = fopen("song_playlist.txt", "a");
  fprintf(playlist, "%s\n", title_str);
  fclose(playlist);

  system("awk '{print $0 | \"sort -f\"}' playlist.txt >> sorted.txt");
  system("rm playlist.txt | mv sorted.txt playlist.txt");
  // sorting isi playlist.txt
}
```

ROT13 decoder
```sh
char* rot13(char* str) {
    char* ctr = str;
    while (*ctr != '\0') {
        if ((*ctr >= 'A' && *ctr <= 'M') || (*ctr >= 'a' && *ctr <= 'm')) {
            *ctr += 13;
            // penggeseran 13 huruf pertama ke depan
        }
        else if ((*ctr >= 'N' && *ctr <= 'Z') || (*ctr >= 'n' && *ctr <= 'z')) {
            *ctr -= 13;
            // penggeseran 13 huruf terakhir ke belakang
        }
        ctr++;
    }
    return str;
}
```
Base64 decoder
```sh
int base64_decode(char *input, int input_len, char *output, int output_len)
{
    int i, j, k;
    unsigned char a, b, c, d;

    for (i = 0, j = 0; i < input_len && j < output_len; i += 4, j += 3)
    {
        a = input[i];
        b = input[i + 1];
        c = input[i + 2];
        d = input[i + 3];

        a = strchr(base64_chars, a) - base64_chars;
        b = strchr(base64_chars, b) - base64_chars;
        c = (c == '=') ? 0 : strchr(base64_chars, c) - base64_chars;
        d = (d == '=') ? 0 : strchr(base64_chars, d) - base64_chars;
        //karakter '=' untuk bagian kosong

        output[j] = (a << 2) | (b >> 4);
        if (c != 0xFF)
        {
            output[j + 1] = (b << 4) | (c >> 2);
            if (d != 0xFF)
            {
                output[j + 2] = (c << 6) | d;
            }
        }
    }

    return j;
}

char *base64Decr(char *base64)
{
    char *plaintext;
    int len;

    len = strlen(base64);
    if (base64[len - 1] == '\n')
    {
        base64[len - 1] = '\0';
        len--;
    }

    int plaintext_len = base64_decode(base64, len, plaintext, 1000);
    plaintext[plaintext_len] = '\0';

    return plaintext;
}
```
Hexadecimal decoder
```sh
void hexDecr(const char *hex_string, char *output_string)
{
    int i, length;
    length = strlen(hex_string);
    //cari panjang string

    // pastikan panjang string hex adalah bilangan genap
    if (length % 2 != 0)
    {
        fprintf(stderr, "Panjang string hex harus genap\n");
        return;
    }

    // konversi string hex menjadi string biasa
    for (i = 0; i < length; i += 2)
    {
        char buffer[3];
        buffer[0] = hex_string[i];
        buffer[1] = hex_string[i + 1];
        buffer[2] = '\0';

        output_string[i / 2] = (char)strtol(buffer, NULL, 16);
    }

    output_string[i / 2] = '\0';
}
```
#### Point 3c
##### Deskripsi Problem:
Sistem stream akan menampilkan daftar lagu yang telah di-decrypt ketika menerima input "LIST" dari user
##### Solusi
Menampilkan sorted playlist yang sudah disimpan di playlist.txt tadi
##### Code
```sh
void showPlaylist() {
  FILE *fp = fopen("song_playlist.txt", "r");
  char buffer[1024];

  if(fp == NULL) {
    printf("Error opening file\n");
    exit(1);
  }

  int count = 0;
  //tampilkan dengan urutan lagu
  while(fgets(buffer, sizeof(buffer), fp) != NULL) {
    char *song = strtok(buffer, "\n");
    printf("%d - %s\n", ++count, buffer);
  }
  fclose(fp);
}
```
#### Point 3d
##### Deskripsi Problem:
User juga dapat mengirimkan perintah PLAY <SONG> 
##### Solusi
Tampilkan list lagu yang sesuai dengan permintaan user (non-case sensitive)
##### Code
```
void handlePlaySong(char *command) {
  FILE *fp;
  char buffer[1024];
  int count = 0, matchCount = 0;

  fp = fopen("song_playlist.txt", "r");
  if(fp == NULL) {
    printf("Error opening file\n");
    exit(1);
  }

  // Count total songs
  while(fgets(buffer, sizeof(buffer), fp) != NULL) {
    count++;
  }
  rewind(fp);

  // cari lagu yang cocock dengan keyword dari user
  while(fgets(buffer, sizeof(buffer), fp) != NULL) {
    char *song = strtok(buffer, "\n");
    if(strcmp(song, command) == 0) {
      matchCount++;
      // printf("%d. USER PLAYING \"%s\"", matchCount, buffer);
      break;
    }
  }

  if(matchCount == 0) {
    printf("THERE IS NO SONG CONTAINING \"%s\"\n", command);
  } else if (matchCount == 1)
  {
    printf("%d. USER PLAYING \"%s\"", matchCount, buffer);
  }
   {
    printf("THERE ARE \"%d\" SONG CONTAINING \"%s\"\n", matchCount, command);
  }
  fclose(fp);
}
```
#### Point 3e
##### Deskripsi Problem
User dapat menambahkan lagu ke dalam list dengan command "ADD"
##### Solusi
Berikan akses untuk menginputkan string baru
##### Code
```sh
void Add()
{
    char *cmd = "awk 'tolower($0) ~ tolower(\"%s\") {print$0}' playlist.txt";
    int j = 4, i = 0;
    char song[50], buffer[50];

    while (j != strlen(message.msg_text) + 1)
    {
        song[i] = message.msg_text[j];
        i++;
        j++;
    }
    song[i] = '\0';
    // printf("%s\n", song);
    if (strlen(song) > 0)
    {
        char playlist[100];
        sprintf(buffer, cmd, song);
        // printf("BUFFER : %s\n", buffer);
        FILE *fp_song = popen(buffer, "r");
        if (fp_song == NULL)
        {
            printf("Failed to execute command\n");
            exit(1);
        }
        // find song
        pid_t pid;
        int status;

        // find song in play list
        fgets(playlist, sizeof(playlist), fp_song);

        //cek keberadaan lagu di dalam playlist
        if (strstr(playlist, song) != NULL)
        {
            printf("SONG ALREADY ON PLAYLIST\n");
        }
        //insert jika tidak ada kecocokan
        else if ((song[0] != '\0') && strstr(playlist, song) == NULL)
        {
            pid = fork();
            if (pid == 0)
            {
                cmd = "echo %s >> playlist.txt";
                sprintf(buffer, cmd, song);
                system(buffer);
                exit(1);
            }
            wait(&status);

            system("awk '{print $0 | \"sort -f\"}' playlist.txt >> sorted.txt");
            system("rm playlist.txt | mv sorted.txt playlist.txt");

            printf("USER <%d> ADD %s\n", message.msg_uid, song);
        }
        //command tidak sesuai ketentuan
        else
        {
            printf("UNKNOWN COMMAND\n");
        }
    }
    else
    {
        printf("UNKNOWN COMMAND\n");
    }
}
```
#### Point 3f
##### Deskripsi Problem
Karena Elshe hanya memiliki resource yang kecil, untuk saat ini Elshe hanya
dapat memiliki dua user. Gunakan semaphore (wajib) untuk membatasi user
yang mengakses playlist.
Output-kan "STREAM SYSTEM OVERLOAD" pada sistem ketika user ketiga
mengirim perintah apapun.
##### Solusi
##### Code
##### Penjelasan

#### Point 3g
##### Deskripsi Problem
Apabila perintahnya tidak dapat dipahami oleh sistem, sistem akan
menampilkan "UNKNOWN COMMAND"
##### Solusi
Tambahkan opsi output "UNKNOWN COMMAND" pada stream
##### Code
```sh
 while(fgets(buf.mtext, sizeof buf.mtext, stdin) != NULL) {
     ...
     ...
     ...
     ...
      else printf("UNKNOWN COMMAND\n");

  }
```
### Problem 4
##### Deskripsi Problem:
Suatu hari, Amin, seorang mahasiswa Informatika mendapati suatu file bernama `hehe.zip`. Di dalam file .zip tersebut, terdapat sebuah folder bernama files dan file .txt bernama `extensions.txt` dan `max.txt`. Setelah melamun beberapa saat, Amin mendapatkan ide untuk merepotkan kalian dengan file tersebut! 

#### Point 4A
##### Deskripsi Problem:
Download dan unzip file tersebut dalam kode c bernama `unzip.c`.

##### Solusi
Mendownload file drive dengan `unzip.c` dan unzip file tersebut.

##### Potongan kode
```
char file_url[] = "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
    char nama_file[] = "hehe.zip";

    child_id = fork();
    if(child_id == 0) {
        execlp("wget", "wget", "-O", nama_file, file_url, NULL);
    }

    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0) {
        execlp("unzip", "unzip", nama_file, NULL);
    }

    while ((wait(&status)) > 0);
    child_id = fork();
    if(child_id == 0) {
        execlp("rm", "rm", nama_file, NULL);
    }
```
##### Penjelasan
1. char *argv[] : untuk mendeklarasikan sebuah array string untuk menyimpan argumen baris perintah yang akan diteruskan ke fungsi execv().
2. wget : sebuah program yang digunakan untuk mengunduh file dari internet.
3. -0 : opsi untuk menentukan nama file output dari unduhan yang dilakukan.
4. execv : untuk menggantikan image program saat ini dengan image program baru yang disebut dalam argumen pertama tadi.
5. & : untuk menjalankan command wget dalam background, sehingga child process dapat berlanjut tanpa menunggu operasi download selesai

#### Point 4B
##### Deskripsi Problem:
Selanjutnya, buatlah program `categorize.c` untuk mengumpulkan (move / copy) file sesuai extension-nya. Extension yang ingin dikumpulkan terdapat dalam file `extensions.txt`. Buatlah folder `categorized` dan di dalamnya setiap extension akan dibuatkan folder dengan nama sesuai nama extension-nya dengan nama folder semua lowercase. Akan tetapi, file bisa saja tidak semua lowercase. File lain dengan extension selain yang terdapat dalam .txt files tersebut akan dimasukkan ke folder other.
Pada file `max.txt`, terdapat angka yang merupakan isi maksimum dari folder tiap extension kecuali folder other. Sehingga, jika penuh, buatlah folder baru dengan format `extension (2)`, `extension (3)`, dan seterusnya.

##### Potongan kode
```
void *logging(){
    /*
    Program tersebut adalah sebuah fungsi bernama "logging" 
    yang digunakan untuk mencatat aktivitas pada direktori dan file dalam file log.txt. 
    
    Fungsi ini menerima beberapa argumen seperti mode, param1, param2, dan param3 
    yang digunakan untuk mencatat informasi terkait aktivitas yang dilakukan. Informasi yang dicatat meliputi waktu akses/move/made, jenis aktivitas, dan lokasi akses/move/made.
    */
    char file_log[] = "log.txt"; // digunakan untuk mencatat aktivitas.

    time_t now = time(NULL); // untuk menyimpan waktu saat ini dalam format yang diinginkan.
    struct tm *t = localtime(&now); // strftime() untuk mengubah waktu saat ini menjadi string dengan format tertentu. String tersebut akan digunakan sebagai bagian dari pesan log yang akan dicatat.
    char datetime[20];
    strftime(datetime, sizeof(datetime), "%d-%m-%Y %H:%M:%S", t);

    // Cek mode yang diberikan sebagai argumen pada fungsi logging. Terdapat tiga mode yaitu 1 (accessed), 2 (moved), dan 3 (made).
    if(mode == 1){
        char mode[] = "ACCESSED"; // membuat sebuah string "mode" dengan isi "ACCESSED"
        char write[MAX_LINE_LENGTH]; // Lalu membuat sebuah string "write" yang berisi pesan log yang akan dicatat
        sprintf(write, "%s %s %s", datetime, mode, param1);

        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
        /*
        String "write" berisi waktu akses, jenis aktivitas, dan lokasi akses. 
        Pesan log tersebut akan dicatat ke dalam file log.txt 
        dengan menggunakan perintah "system(cmd)" dimana "cmd" berisi perintah 
        untuk menulis pesan log tersebut ke dalam file log.txt.
        */
    }else if(mode == 2){
        char mode[] = "MOVED"; // membuat sebuah string "mode" dengan isi "MOVED"
        char write[MAX_LINE_LENGTH]; // Lalu membuat sebuah string "write" yang berisi pesan log yang akan dicatat
        sprintf(write, "%s %s %s file : %s > %s", datetime, mode, param1, param2, param3);

        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
        /*
        String "write" berisi waktu akses, jenis aktivitas, dan lokasi akses. 
        Pesan log tersebut akan dicatat ke dalam file log.txt 
        dengan menggunakan perintah "system(cmd)" dimana "cmd" berisi perintah 
        untuk menulis pesan log tersebut ke dalam file log.txt.
        */
    }else{
        char mode[] = "MADE"; // membuat sebuah string "mode" dengan isi "MADE"
        char write[MAX_LINE_LENGTH]; // Lalu membuat sebuah string "write" yang berisi pesan log yang akan dicatat
        sprintf(write, "%s %s %s", datetime, mode, param1);

        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
        /*
        String "write" berisi waktu akses, jenis aktivitas, dan lokasi akses. 
        Pesan log tersebut akan dicatat ke dalam file log.txt 
        dengan menggunakan perintah "system(cmd)" dimana "cmd" berisi perintah 
        untuk menulis pesan log tersebut ke dalam file log.txt.
        */
    }
}

void create_thread_logging(int modeNew, char param1New[], char param2New[], char param3New[]){
    // Untuk membuat thread yang menjalankan fungsi logging().
    mode = modeNew;
    // set variabel mode, param1, param2, dan param3 sesuai dengan nilai argumen yang diberikan.
    strcpy(param1, param1New);
    strcpy(param2, param2New);
    strcpy(param3, param3New);

    pthread_join(threads[1], NULL); // ungsi akan memanggil pthread_join() pada threads[1] untuk memastikan bahwa thread sebelumnya sudah selesai dijalankan.
    pthread_create(&threads[1], NULL, logging, NULL); // Setelah itu, fungsi akan membuat thread baru dengan memanggil pthread_create()
    pthread_join(threads[1], NULL); // memanggil pthread_join() lagi pada threads[1] untuk memastikan bahwa thread baru sudah selesai dijalankan.
}

const char *get_filename_ext(char *filename) {
    // untuk mendapatkan ekstensi dari sebuah file yang diberikan. 
    // Fungsi ini menerima argumen filename yang merupakan nama file.
    const char *dot = strrchr(filename, '.');
    // menggunakan strrchr untuk mencari titik terakhir pada string filename, yang menandakan awal dari ekstensi file. 
    if(!dot || dot == filename) return "";
    // Jika tidak ditemukan titik atau titik berada pada awal string, maka fungsi mengembalikan string kosong.
    return dot + 1;
    // return dot + 1 akan mengembalikan pointer ke karakter 't', sehingga fungsi akan mengembalikan string "txt".
}

int check_ekstension(char eks[]){
    // Fungsi check_ekstension() digunakan untuk memeriksa apakah sebuah ekstensi file sudah pernah ditemukan sebelumnya. Fungsi ini menerima argumen eks yang merupakan ekstensi file yang akan diperiksa.
    for(int i=0; i<extCount;i++){
        // memeriksa apakah ekstensi yang diberikan sama dengan elemen array yang sedang diiterasi menggunakan fungsi strcmp()
        if(strcmp(eks, ekstension[i]) == 0)
        // Jika ditemukan elemen array yang sama dengan ekstensi yang diberikan, maka fungsi akan mengembalikan indeks elemen array tersebut.
            return i;
    }
    return -1;
    // Jika tidak ditemukan, fungsi akan mengembalikan nilai -1
}

void *createdir(){
    // Fungsi createdir() digunakan untuk membuat direktori categorized dan beberapa direktori lainnya di dalamnya berdasarkan daftar ekstensi file yang terdapat dalam file extensions.txt. Fungsi ini juga mencatat aktivitas pembuatan direktori dalam file log.txt.
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(folder_name, "./categorized"); // membuka file categorized
    // menginisialisasi variabel folder_name dengan string "./categorized" yang akan digunakan sebagai nama folder utama. 
    sprintf(cmd, "mkdir -p %s", folder_name);
    // mkdir dengan argumen "-p" untuk membuat folder tersebut jika belum ada.
    system(cmd);

    create_thread_logging( 3, folder_name, "null", "null");
    // mencatat aktivitas pembuatan direktori dalam file "log.txt".

    strcpy(nama_file, "extensions.txt"); // // membuka file extensions
    int index=0;
    textfile = fopen(nama_file, "r");

    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        line[strlen(line)-2] = '\0';
        // menghapus karakter newline pada akhir baris dengan line[strlen(line)-2] = '\0'.
        sprintf(cmd, "mkdir -p %s/%s", folder_name, line);
        // membuat folder tersebut jika belum ada, dan menyimpan nama ekstensi ke dalam array ekstension.
        strcpy(ekstension[index], line);
        ekstensionDir[index] = 0;
        index++;
        extCount++;
        system(cmd);

        sprintf(buffer, "%s/%s", folder_name, line);
        create_thread_logging(3, buffer, "null", "null");
        // mode "3" dan argumen buffer yang berisi nama folder untuk setiap ekstensi yang baru dibuat.
    }
    fclose(textfile); // tutup extensions.txt
    sprintf(cmd, "mkdir -p %s/other", folder_name);
    system(cmd);

    sprintf(buffer, "%s/other", folder_name);
    create_thread_logging( 3, buffer, "null", "null");
}

void listdir(const char *name)
// Fungsi listdir() digunakan untuk mencatat daftar file dalam direktori dan subdirektori tertentu dalam file listFiles.txt. 
// Fungsi ini menerima argumen name yang merupakan nama direktori.
{
    DIR *dir;
    struct dirent *entry;
    char cmd[MAX_LINE_LENGTH];

    if (!(dir = opendir(name)))
    // membuka direktori yang ditentukan menggunakan fungsi opendir()
        return;

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR) { // Jika entry yang dibaca adalah sebuah direktori
            char path[1024];
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
            create_thread_logging(1, path, "null", "null");
            listdir(path); // maka fungsi akan melakukan rekursi untuk mengecek 
            // daftar file dalam direktori tersebut dengan memanggil kembali fungsi listdir() 
        } else { // Jika entry yang dibaca adalah sebuah file
            sprintf(cmd, "echo '%s/%s' >> listFiles.txt", name,entry->d_name);
            // mencatat path file tersebut ke dalam file listFiles.txt menggunakan perintah echo dan system()
            system(cmd);
        }
    }
    closedir(dir);
}


void *listfile(){
    // Fungsi listfile() digunakan untuk menjalankan fungsi listdir() pada direktori ./files. Fungsi ini juga membaca nilai maksimum yang diperbolehkan untuk jumlah file dalam direktori dan subdirektori tertentu dari file max.txt.
    char charMax [3];
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(nama_file, "max.txt"); // membaca file max.txt yang berisi nilai maksimum yang 
    // diperbolehkan untuk jumlah file dalam direktori dan subdirektori tertentu.
    textfile = fopen (nama_file, "r"); fgets (charMax, 3, textfile); fclose(textfile);
    max = atoi(charMax);

    listdir("./files"); // untuk mencatat daftar file dalam direktori dan subdirektori tertentu dalam file listFiles.txt
}

void *movefile(){
    // Fungsi movefile() digunakan untuk memindahkan file-file dalam direktori dan subdirektori tertentu ke dalam direktori sesuai dengan ekstensinya. Pemindahan dilakukan berdasarkan daftar file yang terdapat dalam file listFiles.txt. Fungsi ini juga mencatat aktivitas pemindahan file dalam file log.txt.
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(nama_file, "listFiles.txt"); 
    // membuka file listFiles.txt dan membacanya secara baris per baris menggunakan fungsi fgets().
    textfile = fopen(nama_file, "r");     
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        line[strlen(line)-1] = '\0';
        char eks[MAX_LINE_LENGTH];
        sprintf(eks,"%s",get_filename_ext(line)); // untuk mendapatkan ekstensi dari file tersebut.
        for(int i = 0; eks[i]; i++){
            eks[i] = tolower(eks[i]); // ekstensi yang didapat akan diubah menjadi huruf kecil menggunakan fungsi tolower()
        }
        if(check_ekstension(eks) == -1){
            // jika ekstensi tidak terdaftar dalam daftar ekstensi yang diterima
            sprintf(cmd, "cp '%s' ./categorized/other", line);
            // maka file akan dipindahkan ke direktori "other" dalam folder categorized
            system(cmd); // menggunakan perintah "cp" dan dicatat aktivitasnya pada file log.txt menggunakan fungsi create_thread_logging().
            create_thread_logging(2, "other", line,"./categorized/other" );
        }
        else{ // Jika ekstensi terdaftar dalam daftar ekstensi yang diterima
        // program akan mengecek apakah direktori untuk ekstensi tersebut masih dapat menampung file atau tidak. 
            if(ekstensionDir[check_ekstension(eks)] < max){ // ika masih dapat menampung
                sprintf(cmd, "cp '%s' ./categorized/%s", line, eks);
                // file akan dipindahkan ke direktori yang sesuai dengan ekstensinya menggunakan perintah "cp"
                system(cmd);
                ekstensionDir[check_ekstension(eks)]++;
                // dicatat aktivitasnya pada file log.txt menggunakan fungsi create_thread_logging(). 
                sprintf(buffer, "./categorized/%s", eks);
                create_thread_logging(2, eks, line, buffer );
            }else{ // Jika sudah tidak dapat menampung
                int num = ekstensionDir[check_ekstension(eks)] / max + 1;
                // program akan membuat direktori baru dengan nama "<ekstensi> <nomor>"
                // Setelah semua file dipindahkan, program akan selesai dan kontrol dikembalikan ke fungsi yang memanggilnya.
                sprintf(buffer, "./categorized/%s %d", eks, num);
                if(access(buffer, F_OK) != 0){
                    create_thread_logging(3,buffer, "null", "null" );
                }
                sprintf(cmd, "mkdir -p './categorized/%s %d'", eks, num);
                system(cmd);

                sprintf(cmd, "cp '%s' './categorized/%s %d'", line, eks, num);
                system(cmd);
                ekstensionDir[check_ekstension(eks)]++;

                sprintf(buffer, "./categorized/%s %d", eks, num);
                create_thread_logging(2, eks, line, buffer );
            }
        }
    }
}

void *count_eks(){
    // fungsi yang tidak mengembalikan nilai (void) dan akan dijalankan sebagai thread. 
    // Fungsi ini bertujuan untuk menghitung jumlah file yang ditemukan pada setiap direktori dengan ekstensi tertentu.
    strcpy(ekstension[extCount], "other");
    // menambahkan other ke dalam daftar ekstensi (ekstension) yang ada pada program. 
    // other digunakan untuk menyimpan file yang tidak memiliki ekstensi tertentu.
    DIR *directory;
    struct dirent *entry;
    directory = opendir("./categorized/other"); // direktori yang berisi file-file yang tidak memiliki ekstensi tertentu.

    create_thread_logging(1, "./categorized/other", "null", "null");

    while ((entry = readdir(directory)) != NULL) {
        if (entry->d_type == DT_REG) {
            ekstensionDir[extCount]++;
        }
    }

    closedir(directory);
    extCount++;
    // Bubble Sort
    for(int i=0;i<extCount; i++){
        for(int j=0; j<extCount-1;j++){
            if(ekstensionDir[j] > ekstensionDir[j+1]){
                int temp;
                temp = ekstensionDir[j];
                ekstensionDir[j] = ekstensionDir[j+1];
                ekstensionDir[j+1] = temp;

                strcpy(buffer, ekstension[j]);
                strcpy(ekstension[j], ekstension[j+1]);
                strcpy(ekstension[j+1], buffer);

            }
        }
    }
    
    for(int i=0; i<extCount; i++){ // menampilkan hasil
        printf("%s : %d\n", ekstension[i], ekstensionDir[i]);
    }
}
```
##### Penjelasan
1. trftime() : mengubah waktu saat ini menjadi string dengan format tertentu. String tersebut akan digunakan sebagai bagian dari pesan log yang akan dicatat.
2. Fungsi logging digunakan untuk mencatat aktivitas pada direktori dan file dalam file log.txt. 
3. Fungsi create_thread_logging Untuk membuat thread yang menjalankan fungsi logging().
4. Fungsi *get_filename_ext untuk menerima argumen filename yang merupakan nama file.
5. Fungsi check_ekstension untuk memeriksa apakah sebuah ekstensi file sudah pernah ditemukan sebelumnya. Fungsi ini menerima argumen eks yang merupakan ekstensi file yang akan diperiksa.
6. Fungsi *createdir untuk membuat direktori categorized dan beberapa direktori lainnya di dalamnya berdasarkan daftar ekstensi file yang terdapat dalam file extensions.txt. Fungsi ini juga mencatat aktivitas pembuatan direktori dalam file log.txt.
7. Fungsi listdir() untuk mencatat daftar file dalam direktori dan subdirektori tertentu dalam file listFiles.txt. 
8. Fungsi listfile() untuk menjalankan fungsi listdir() pada direktori ./files. Fungsi ini juga membaca nilai maksimum yang diperbolehkan untuk jumlah file dalam direktori dan subdirektori tertentu dari file max.txt.
9. Fungsi movefile() untuk memindahkan file-file dalam direktori dan subdirektori tertentu ke dalam direktori sesuai dengan ekstensinya. Pemindahan dilakukan berdasarkan daftar file yang terdapat dalam file listFiles.txt. Fungsi ini juga mencatat aktivitas pemindahan file dalam file log.txt.
10. Fungsi count_eks untuk menghitung jumlah file yang ditemukan pada setiap direktori dengan ekstensi tertentu.

#### Point 4C
##### Deskripsi Problem:
Output-kan pada terminal banyaknya file tiap extension terurut ascending dengan semua lowercase, beserta other juga dengan format sebagai berikut.
`extension_a : banyak_file`
`extension_b : banyak_file`
`extension_c : banyak_file`
`other : banyak_file`
##### Solusi
Print jumlah file yang aad di dalam 1 direktori.
##### Potongan kode
```
for(int i=0;i<extCount; i++){
        for(int j=0; j<extCount-1;j++){
            if(ekstensionDir[j] > ekstensionDir[j+1]){
                int temp;
                temp = ekstensionDir[j];
                ekstensionDir[j] = ekstensionDir[j+1];
                ekstensionDir[j+1] = temp;

                strcpy(buffer, ekstension[j]);
                strcpy(ekstension[j], ekstension[j+1]);
                strcpy(ekstension[j+1], buffer);

            }
        }
    }
    
    for(int i=0; i<extCount; i++){ // menampilkan hasil
        printf("%s : %d\n", ekstension[i], ekstensionDir[i]);
    }
```
##### Penjelasan
1. Print jumlah file yang aad di dalam 1 direktori.

#### Point 4D
##### Deskripsi Problem:
Setiap pengaksesan folder, sub-folder, dan semua folder pada program `categorize.c` wajib menggunakan multithreading. Jika tidak menggunakan akan ada pengurangan nilai.

##### Solusi
Mendownload file drive dengan `unzip.c` dan unzip file tersebut.

##### Potongan kode
```
void *logging(){
    /*
    Program tersebut adalah sebuah fungsi bernama "logging" 
    yang digunakan untuk mencatat aktivitas pada direktori dan file dalam file log.txt. 
    
    Fungsi ini menerima beberapa argumen seperti mode, param1, param2, dan param3 
    yang digunakan untuk mencatat informasi terkait aktivitas yang dilakukan. Informasi yang dicatat meliputi waktu akses/move/made, jenis aktivitas, dan lokasi akses/move/made.
    */
    char file_log[] = "log.txt"; // digunakan untuk mencatat aktivitas.

    time_t now = time(NULL); // untuk menyimpan waktu saat ini dalam format yang diinginkan.
    struct tm *t = localtime(&now); // strftime() untuk mengubah waktu saat ini menjadi string dengan format tertentu. String tersebut akan digunakan sebagai bagian dari pesan log yang akan dicatat.
    char datetime[20];
    strftime(datetime, sizeof(datetime), "%d-%m-%Y %H:%M:%S", t);

    // Cek mode yang diberikan sebagai argumen pada fungsi logging. Terdapat tiga mode yaitu 1 (accessed), 2 (moved), dan 3 (made).
    if(mode == 1){
        char mode[] = "ACCESSED"; // membuat sebuah string "mode" dengan isi "ACCESSED"
        char write[MAX_LINE_LENGTH]; // Lalu membuat sebuah string "write" yang berisi pesan log yang akan dicatat
        sprintf(write, "%s %s %s", datetime, mode, param1);

        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
        /*
        String "write" berisi waktu akses, jenis aktivitas, dan lokasi akses. 
        Pesan log tersebut akan dicatat ke dalam file log.txt 
        dengan menggunakan perintah "system(cmd)" dimana "cmd" berisi perintah 
        untuk menulis pesan log tersebut ke dalam file log.txt.
        */
    }else if(mode == 2){
        char mode[] = "MOVED"; // membuat sebuah string "mode" dengan isi "MOVED"
        char write[MAX_LINE_LENGTH]; // Lalu membuat sebuah string "write" yang berisi pesan log yang akan dicatat
        sprintf(write, "%s %s %s file : %s > %s", datetime, mode, param1, param2, param3);

        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
        /*
        String "write" berisi waktu akses, jenis aktivitas, dan lokasi akses. 
        Pesan log tersebut akan dicatat ke dalam file log.txt 
        dengan menggunakan perintah "system(cmd)" dimana "cmd" berisi perintah 
        untuk menulis pesan log tersebut ke dalam file log.txt.
        */
    }else{
        char mode[] = "MADE"; // membuat sebuah string "mode" dengan isi "MADE"
        char write[MAX_LINE_LENGTH]; // Lalu membuat sebuah string "write" yang berisi pesan log yang akan dicatat
        sprintf(write, "%s %s %s", datetime, mode, param1);

        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
        /*
        String "write" berisi waktu akses, jenis aktivitas, dan lokasi akses. 
        Pesan log tersebut akan dicatat ke dalam file log.txt 
        dengan menggunakan perintah "system(cmd)" dimana "cmd" berisi perintah 
        untuk menulis pesan log tersebut ke dalam file log.txt.
        */
    }
}

void create_thread_logging(int modeNew, char param1New[], char param2New[], char param3New[]){
    // Untuk membuat thread yang menjalankan fungsi logging().
    mode = modeNew;
    // set variabel mode, param1, param2, dan param3 sesuai dengan nilai argumen yang diberikan.
    strcpy(param1, param1New);
    strcpy(param2, param2New);
    strcpy(param3, param3New);

    pthread_join(threads[1], NULL); // ungsi akan memanggil pthread_join() pada threads[1] untuk memastikan bahwa thread sebelumnya sudah selesai dijalankan.
    pthread_create(&threads[1], NULL, logging, NULL); // Setelah itu, fungsi akan membuat thread baru dengan memanggil pthread_create()
    pthread_join(threads[1], NULL); // memanggil pthread_join() lagi pada threads[1] untuk memastikan bahwa thread baru sudah selesai dijalankan.
}

const char *get_filename_ext(char *filename) {
    // untuk mendapatkan ekstensi dari sebuah file yang diberikan. 
    // Fungsi ini menerima argumen filename yang merupakan nama file.
    const char *dot = strrchr(filename, '.');
    // menggunakan strrchr untuk mencari titik terakhir pada string filename, yang menandakan awal dari ekstensi file. 
    if(!dot || dot == filename) return "";
    // Jika tidak ditemukan titik atau titik berada pada awal string, maka fungsi mengembalikan string kosong.
    return dot + 1;
    // return dot + 1 akan mengembalikan pointer ke karakter 't', sehingga fungsi akan mengembalikan string "txt".
}

int check_ekstension(char eks[]){
    // Fungsi check_ekstension() digunakan untuk memeriksa apakah sebuah ekstensi file sudah pernah ditemukan sebelumnya. Fungsi ini menerima argumen eks yang merupakan ekstensi file yang akan diperiksa.
    for(int i=0; i<extCount;i++){
        // memeriksa apakah ekstensi yang diberikan sama dengan elemen array yang sedang diiterasi menggunakan fungsi strcmp()
        if(strcmp(eks, ekstension[i]) == 0)
        // Jika ditemukan elemen array yang sama dengan ekstensi yang diberikan, maka fungsi akan mengembalikan indeks elemen array tersebut.
            return i;
    }
    return -1;
    // Jika tidak ditemukan, fungsi akan mengembalikan nilai -1
}

void *createdir(){
    // Fungsi createdir() digunakan untuk membuat direktori categorized dan beberapa direktori lainnya di dalamnya berdasarkan daftar ekstensi file yang terdapat dalam file extensions.txt. Fungsi ini juga mencatat aktivitas pembuatan direktori dalam file log.txt.
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(folder_name, "./categorized"); // membuka file categorized
    // menginisialisasi variabel folder_name dengan string "./categorized" yang akan digunakan sebagai nama folder utama. 
    sprintf(cmd, "mkdir -p %s", folder_name);
    // mkdir dengan argumen "-p" untuk membuat folder tersebut jika belum ada.
    system(cmd);

    create_thread_logging( 3, folder_name, "null", "null");
    // mencatat aktivitas pembuatan direktori dalam file "log.txt".

    strcpy(nama_file, "extensions.txt"); // // membuka file extensions
    int index=0;
    textfile = fopen(nama_file, "r");

    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        line[strlen(line)-2] = '\0';
        // menghapus karakter newline pada akhir baris dengan line[strlen(line)-2] = '\0'.
        sprintf(cmd, "mkdir -p %s/%s", folder_name, line);
        // membuat folder tersebut jika belum ada, dan menyimpan nama ekstensi ke dalam array ekstension.
        strcpy(ekstension[index], line);
        ekstensionDir[index] = 0;
        index++;
        extCount++;
        system(cmd);

        sprintf(buffer, "%s/%s", folder_name, line);
        create_thread_logging(3, buffer, "null", "null");
        // mode "3" dan argumen buffer yang berisi nama folder untuk setiap ekstensi yang baru dibuat.
    }
    fclose(textfile); // tutup extensions.txt
    sprintf(cmd, "mkdir -p %s/other", folder_name);
    system(cmd);

    sprintf(buffer, "%s/other", folder_name);
    create_thread_logging( 3, buffer, "null", "null");
}

void listdir(const char *name)
// Fungsi listdir() digunakan untuk mencatat daftar file dalam direktori dan subdirektori tertentu dalam file listFiles.txt. 
// Fungsi ini menerima argumen name yang merupakan nama direktori.
{
    DIR *dir;
    struct dirent *entry;
    char cmd[MAX_LINE_LENGTH];

    if (!(dir = opendir(name)))
    // membuka direktori yang ditentukan menggunakan fungsi opendir()
        return;

    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR) { // Jika entry yang dibaca adalah sebuah direktori
            char path[1024];
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
            create_thread_logging(1, path, "null", "null");
            listdir(path); // maka fungsi akan melakukan rekursi untuk mengecek 
            // daftar file dalam direktori tersebut dengan memanggil kembali fungsi listdir() 
        } else { // Jika entry yang dibaca adalah sebuah file
            sprintf(cmd, "echo '%s/%s' >> listFiles.txt", name,entry->d_name);
            // mencatat path file tersebut ke dalam file listFiles.txt menggunakan perintah echo dan system()
            system(cmd);
        }
    }
    closedir(dir);
}


void *listfile(){
    // Fungsi listfile() digunakan untuk menjalankan fungsi listdir() pada direktori ./files. Fungsi ini juga membaca nilai maksimum yang diperbolehkan untuk jumlah file dalam direktori dan subdirektori tertentu dari file max.txt.
    char charMax [3];
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(nama_file, "max.txt"); // membaca file max.txt yang berisi nilai maksimum yang 
    // diperbolehkan untuk jumlah file dalam direktori dan subdirektori tertentu.
    textfile = fopen (nama_file, "r"); fgets (charMax, 3, textfile); fclose(textfile);
    max = atoi(charMax);

    listdir("./files"); // untuk mencatat daftar file dalam direktori dan subdirektori tertentu dalam file listFiles.txt
}

void *movefile(){
    // Fungsi movefile() digunakan untuk memindahkan file-file dalam direktori dan subdirektori tertentu ke dalam direktori sesuai dengan ekstensinya. Pemindahan dilakukan berdasarkan daftar file yang terdapat dalam file listFiles.txt. Fungsi ini juga mencatat aktivitas pemindahan file dalam file log.txt.
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(nama_file, "listFiles.txt"); 
    // membuka file listFiles.txt dan membacanya secara baris per baris menggunakan fungsi fgets().
    textfile = fopen(nama_file, "r");     
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        line[strlen(line)-1] = '\0';
        char eks[MAX_LINE_LENGTH];
        sprintf(eks,"%s",get_filename_ext(line)); // untuk mendapatkan ekstensi dari file tersebut.
        for(int i = 0; eks[i]; i++){
            eks[i] = tolower(eks[i]); // ekstensi yang didapat akan diubah menjadi huruf kecil menggunakan fungsi tolower()
        }
        if(check_ekstension(eks) == -1){
            // jika ekstensi tidak terdaftar dalam daftar ekstensi yang diterima
            sprintf(cmd, "cp '%s' ./categorized/other", line);
            // maka file akan dipindahkan ke direktori "other" dalam folder categorized
            system(cmd); // menggunakan perintah "cp" dan dicatat aktivitasnya pada file log.txt menggunakan fungsi create_thread_logging().
            create_thread_logging(2, "other", line,"./categorized/other" );
        }
        else{ // Jika ekstensi terdaftar dalam daftar ekstensi yang diterima
        // program akan mengecek apakah direktori untuk ekstensi tersebut masih dapat menampung file atau tidak. 
            if(ekstensionDir[check_ekstension(eks)] < max){ // ika masih dapat menampung
                sprintf(cmd, "cp '%s' ./categorized/%s", line, eks);
                // file akan dipindahkan ke direktori yang sesuai dengan ekstensinya menggunakan perintah "cp"
                system(cmd);
                ekstensionDir[check_ekstension(eks)]++;
                // dicatat aktivitasnya pada file log.txt menggunakan fungsi create_thread_logging(). 
                sprintf(buffer, "./categorized/%s", eks);
                create_thread_logging(2, eks, line, buffer );
            }else{ // Jika sudah tidak dapat menampung
                int num = ekstensionDir[check_ekstension(eks)] / max + 1;
                // program akan membuat direktori baru dengan nama "<ekstensi> <nomor>"
                // Setelah semua file dipindahkan, program akan selesai dan kontrol dikembalikan ke fungsi yang memanggilnya.
                sprintf(buffer, "./categorized/%s %d", eks, num);
                if(access(buffer, F_OK) != 0){
                    create_thread_logging(3,buffer, "null", "null" );
                }
                sprintf(cmd, "mkdir -p './categorized/%s %d'", eks, num);
                system(cmd);

                sprintf(cmd, "cp '%s' './categorized/%s %d'", line, eks, num);
                system(cmd);
                ekstensionDir[check_ekstension(eks)]++;

                sprintf(buffer, "./categorized/%s %d", eks, num);
                create_thread_logging(2, eks, line, buffer );
            }
        }
    }
}

void *count_eks(){
    // fungsi yang tidak mengembalikan nilai (void) dan akan dijalankan sebagai thread. 
    // Fungsi ini bertujuan untuk menghitung jumlah file yang ditemukan pada setiap direktori dengan ekstensi tertentu.
    strcpy(ekstension[extCount], "other");
    // menambahkan other ke dalam daftar ekstensi (ekstension) yang ada pada program. 
    // other digunakan untuk menyimpan file yang tidak memiliki ekstensi tertentu.
    DIR *directory;
    struct dirent *entry;
    directory = opendir("./categorized/other"); // direktori yang berisi file-file yang tidak memiliki ekstensi tertentu.

    create_thread_logging(1, "./categorized/other", "null", "null");

    while ((entry = readdir(directory)) != NULL) {
        if (entry->d_type == DT_REG) {
            ekstensionDir[extCount]++;
        }
    }

    closedir(directory);
    extCount++;
    // Bubble Sort
    for(int i=0;i<extCount; i++){
        for(int j=0; j<extCount-1;j++){
            if(ekstensionDir[j] > ekstensionDir[j+1]){
                int temp;
                temp = ekstensionDir[j];
                ekstensionDir[j] = ekstensionDir[j+1];
                ekstensionDir[j+1] = temp;

                strcpy(buffer, ekstension[j]);
                strcpy(ekstension[j], ekstension[j+1]);
                strcpy(ekstension[j+1], buffer);

            }
        }
    }
    
    for(int i=0; i<extCount; i++){ // menampilkan hasil
        printf("%s : %d\n", ekstension[i], ekstensionDir[i]);
    }
}
```
##### Penjelasan
1. trftime() : mengubah waktu saat ini menjadi string dengan format tertentu. String tersebut akan digunakan sebagai bagian dari pesan log yang akan dicatat.
2. Fungsi logging digunakan untuk mencatat aktivitas pada direktori dan file dalam file log.txt. 
3. Fungsi create_thread_logging Untuk membuat thread yang menjalankan fungsi logging().
4. Fungsi *get_filename_ext untuk menerima argumen filename yang merupakan nama file.
5. Fungsi check_ekstension untuk memeriksa apakah sebuah ekstensi file sudah pernah ditemukan sebelumnya. Fungsi ini menerima argumen eks yang merupakan ekstensi file yang akan diperiksa.
6. Fungsi *createdir untuk membuat direktori categorized dan beberapa direktori lainnya di dalamnya berdasarkan daftar ekstensi file yang terdapat dalam file extensions.txt. Fungsi ini juga mencatat aktivitas pembuatan direktori dalam file log.txt.
7. Fungsi listdir() untuk mencatat daftar file dalam direktori dan subdirektori tertentu dalam file listFiles.txt. 
8. Fungsi listfile() untuk menjalankan fungsi listdir() pada direktori ./files. Fungsi ini juga membaca nilai maksimum yang diperbolehkan untuk jumlah file dalam direktori dan subdirektori tertentu dari file max.txt.
9. Fungsi movefile() untuk memindahkan file-file dalam direktori dan subdirektori tertentu ke dalam direktori sesuai dengan ekstensinya. Pemindahan dilakukan berdasarkan daftar file yang terdapat dalam file listFiles.txt. Fungsi ini juga mencatat aktivitas pemindahan file dalam file log.txt.
10. Fungsi count_eks untuk menghitung jumlah file yang ditemukan pada setiap direktori dengan ekstensi tertentu.

#### Point 4E
##### Deskripsi Problem:
Dalam setiap pengaksesan folder, pemindahan file, pembuatan folder pada program categorize.c buatlah log dengan format sebagai berikut.

DD-MM-YYYY HH:MM:SS ACCESSED [folder path]

DD-MM-YYYY HH:MM:SS MOVED [extension] file : [src path] > [folder dst]

DD-MM-YYYY HH:MM:SS MADE [folder name]

##### Solusi
Print aktivitas pada direktori dan file dalam file log.txt. 

##### Potongan kode
```
void *logging(){
    /*
    Program tersebut adalah sebuah fungsi bernama "logging" 
    yang digunakan untuk mencatat aktivitas pada direktori dan file dalam file log.txt. 
    
    Fungsi ini menerima beberapa argumen seperti mode, param1, param2, dan param3 
    yang digunakan untuk mencatat informasi terkait aktivitas yang dilakukan. Informasi yang dicatat meliputi waktu akses/move/made, jenis aktivitas, dan lokasi akses/move/made.
    */
    char file_log[] = "log.txt"; // digunakan untuk mencatat aktivitas.

    time_t now = time(NULL); // untuk menyimpan waktu saat ini dalam format yang diinginkan.
    struct tm *t = localtime(&now); // strftime() untuk mengubah waktu saat ini menjadi string dengan format tertentu. String tersebut akan digunakan sebagai bagian dari pesan log yang akan dicatat.
    char datetime[20];
    strftime(datetime, sizeof(datetime), "%d-%m-%Y %H:%M:%S", t);

    // Cek mode yang diberikan sebagai argumen pada fungsi logging. Terdapat tiga mode yaitu 1 (accessed), 2 (moved), dan 3 (made).
    if(mode == 1){
        char mode[] = "ACCESSED"; // membuat sebuah string "mode" dengan isi "ACCESSED"
        char write[MAX_LINE_LENGTH]; // Lalu membuat sebuah string "write" yang berisi pesan log yang akan dicatat
        sprintf(write, "%s %s %s", datetime, mode, param1);

        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
        /*
        String "write" berisi waktu akses, jenis aktivitas, dan lokasi akses. 
        Pesan log tersebut akan dicatat ke dalam file log.txt 
        dengan menggunakan perintah "system(cmd)" dimana "cmd" berisi perintah 
        untuk menulis pesan log tersebut ke dalam file log.txt.
        */
    }else if(mode == 2){
        char mode[] = "MOVED"; // membuat sebuah string "mode" dengan isi "MOVED"
        char write[MAX_LINE_LENGTH]; // Lalu membuat sebuah string "write" yang berisi pesan log yang akan dicatat
        sprintf(write, "%s %s %s file : %s > %s", datetime, mode, param1, param2, param3);

        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
        /*
        String "write" berisi waktu akses, jenis aktivitas, dan lokasi akses. 
        Pesan log tersebut akan dicatat ke dalam file log.txt 
        dengan menggunakan perintah "system(cmd)" dimana "cmd" berisi perintah 
        untuk menulis pesan log tersebut ke dalam file log.txt.
        */
    }else{
        char mode[] = "MADE"; // membuat sebuah string "mode" dengan isi "MADE"
        char write[MAX_LINE_LENGTH]; // Lalu membuat sebuah string "write" yang berisi pesan log yang akan dicatat
        sprintf(write, "%s %s %s", datetime, mode, param1);

        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
        /*
        String "write" berisi waktu akses, jenis aktivitas, dan lokasi akses. 
        Pesan log tersebut akan dicatat ke dalam file log.txt 
        dengan menggunakan perintah "system(cmd)" dimana "cmd" berisi perintah 
        untuk menulis pesan log tersebut ke dalam file log.txt.
        */
    }
}
```

##### Solusi
Print aktivitas pada direktori dan file dalam file log.txt. 
Fungsi ini menerima beberapa argumen seperti mode, param1, param2, dan param3 yang digunakan untuk mencatat informasi terkait aktivitas yang dilakukan. Informasi yang dicatat meliputi waktu akses/move/made, jenis aktivitas, dan lokasi akses/move/made.

#### Point 4F
##### Deskripsi Problem:
Untuk mengecek apakah log-nya benar, buatlah suatu program baru dengan nama `logchecker.c` untuk mengekstrak informasi dari `log.txt` dengan ketentuan sebagai berikut.
1. Untuk menghitung banyaknya ACCESSED yang dilakukan.
2. Untuk membuat list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending.
3. Untuk menghitung banyaknya total file tiap extension, terurut secara ascending.

##### Solusi
Menghitung banyaknya ACCESSED yang dilakukan dan mengueutkan list seluruh folder yang telah dibuat dan banyaknya file yang dikumpulkan ke folder tersebut, terurut secara ascending, dan hitung setiap file extention.

##### Potongan kode
```
int count_accessed(){
    // untuk menghitung berapa banyak file yang diakses berdasarkan isi dari file log.txt.
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];
    int counter = 0;
    strcpy(nama_file, "log.txt"); 
    textfile = fopen(nama_file, "r"); // menggunakan fopen() dan membaca isi file tersebut menggunakan fgets()
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        if (strstr(line, "ACCESSED") != NULL) { // mencari kata kunci "ACCESSED". 
            counter++;
            // Jika kata kunci tersebut ditemukan dalam baris yang dibaca,
            //  maka variabel counter akan ditambahkan 1.
        }

    }
    fclose(textfile); // menutup file log.txt 
    return counter;
}

int check_dir(char dirs[] ){
    // Fungsi check_dir akan melakukan pencarian pada array dir
    for(int i=0; i<indexDir;i++){
        if(strcmp(dirs, dir[i]) == 0) // jika ditemukan elemen pada array yang sama dengan dirs
            return i; // maka akan mengembalikan indeks elemen tersebut di dalam array. 
    }
    return -1; // Namun jika tidak ditemukan, maka fungsi akan mengembalikan nilai -1.
}

int check_eks(char ekss[] ){
    // mencari elemen array yang sama dengan ekss
    for(int i=0; i<indexDir;i++){
        if(strcmp(ekss, eks[i]) == 0) 
            return i; // dan mengembalikan indeksnya jika ditemukan
    }
    return -1; // dan mengembalikan nilai -1 jika tidak.
}

void all_dir_count(){

    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(nama_file, "log.txt");
    textfile = fopen(nama_file, "r");     
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        if (strstr(line, "MADE ") != NULL) {
            line[strlen(line)-1] = '\0';
            char *pos = strstr(line, "MADE ");
            char *file = pos + strlen("MADE ");

            strcpy(dir[indexDir], file);
            dirCount[indexDir] = 0;
            indexDir++;
        }
    }
    fclose(textfile);

    strcpy(nama_file, "log.txt");
    textfile = fopen(nama_file, "r"); 
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        if (strstr(line, "> ") != NULL) {
                line[strlen(line)-1] = '\0';
                char *pos = strstr(line, "> ");
                char *file = pos + strlen("> ");

                int num = check_dir(file);
                dirCount[num]++;
        }
    }
    fclose(textfile);


    for(int i=0;i<indexDir; i++){
        for(int j=0; j<indexDir-1;j++){
            if(dirCount[j] > dirCount[j+1]){
                int temp;
                temp = dirCount[j];
                dirCount[j] = dirCount[j+1];
                dirCount[j+1] = temp;

                char buffer[MAX_LINE_LENGTH];
                strcpy(buffer, dir[j]);
                strcpy(dir[j], dir[j+1]);
                strcpy(dir[j+1], buffer);

            }
        }
    }
    
    for(int i=0; i<indexDir; i++){
        printf("%s : %d\n", dir[i], dirCount[i]);
    }
    
}

void all_eks_count(){
    // untuk menghitung jumlah file untuk setiap ekstensi file yang ada 
    // pada sebuah direktori dan menampilkannya ke layar.
    FILE *textfile; // sebagai pointer ke file.
    char line[MAX_LINE_LENGTH]; // sebagai variabel untuk menyimpan baris yang dibaca dari file
    char nama_file[MAX_LINE_LENGTH]; // sebagai nama file yang akan dibaca.

    strcpy(nama_file, "extensions.txt");
    // membuka file yang bernama extensions.txt dengan mode "r"
    textfile = fopen(nama_file, "r");
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        // membaca isi file baris per baris menggunakan fungsi
        line[strlen(line)-2] = '\0'; // menghapus karakter "\n" pada setiap baris yang dibaca dari file 
        // menggantinya dengan karakter nul '\0' 
        // sehingga tidak mengganggu saat mengecek ekstensi file pada program selanjutnya.
        strcpy(eks[indexEks], line); // disimpan ke dalam array eks
        eksCount[indexEks] = 0; // umlah file untuk setiap ekstensi diset menjadi 0 pada array eksCount.
        indexEks++;
    }
    fclose(textfile);
    strcpy(eks[indexEks], "other");
    eksCount[indexEks]=0;
    indexEks++;

    strcpy(nama_file, "log.txt");
    textfile = fopen(nama_file, "r"); 
    while(fgets(line, MAX_LINE_LENGTH, textfile)){ // mengecek setiap baris pada file.
        if (strstr(line, "MOVED ") != NULL) { 
            line[strlen(line)-1] = '\0';
            char *start = strstr(line, "MOVED "); // mencari kata "MOVED " pada setiap baris yang dibaca dari file log.
            char *en = strstr(start, " file :"); //  program akan mencari kata " file :" pada baris yang sama
            char file[MAX_LINE_LENGTH];
            strncpy(file, start + strlen("MOVED "), en - start - strlen("MOVED ")); // copy
            file[en - start - strlen("MOVED ")] = '\0';
            // ditambahkan pada akhir variabel file agar tidak terjadi error pada program selanjutnya.

            int num = check_eks(file); // mengecek apakah ekstensi file yang terdapat pada variabel file
            // sudah ada pada array eks atau belum menggunakan fungsi check_eks().
            eksCount[num]++;
        }
    }
    fclose(textfile); // file tersebut akan ditutup 

    // mengurutkan elemen-elemen dalam sebuah array.
    for(int i=0;i<indexEks; i++){
        // untuk menelusuri seluruh elemen dalam array eksCount
        for(int j=0; j<indexEks-1;j++){
            //  untuk membandingkan nilai elemen pada indeks ke-j dan indeks ke-j+1
            if(eksCount[j] > eksCount[j+1]){ // memeriksa apakah eksCount[j] > eksCount[j+1].
                int temp;
                temp = eksCount[j];
                eksCount[j] = eksCount[j+1];
                eksCount[j+1] = temp;
                // maka nilai eksCount[j] dan eksCount[j+1] ditukar tempat

                char buffer[MAX_LINE_LENGTH];
                strcpy(buffer, eks[j]);
                strcpy(eks[j], eks[j+1]);
                strcpy(eks[j+1], buffer);

            }
        }
    }
    
    for(int i=0; i<indexEks; i++){ // tampilkan
        printf("%s : %d\n", eks[i], eksCount[i]);
    }
}
```
##### Penjelasan
1. Fungsi count_accessed untuk menghitung berapa banyak file yang diakses berdasarkan isi dari file `log.txt`.
2. Fungsi check_dir untuk melakukan pencarian pada array dir.
3. Fungsi check_eks untuk mencari elemen array yang sama dengan ekss.
4. Fungsi all_eks_count untuk menghitung jumlah file untuk setiap ekstensi file yang ada.
