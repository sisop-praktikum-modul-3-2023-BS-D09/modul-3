#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <time.h>
#include <pthread.h>


unsigned long long *temp; // variabel hasil nantinya digunakan untuk menyimpan hasil operasi faktorial dari setiap elemen matriks.
unsigned long long factorial(unsigned long long n)
// fungsi menghitung faktorial dari sebuah bilangan n.
{
  if(n == 0) return 1; // jika n = 0(faktorial dari 0), hasilnya return 1
  else
  {
      return n * factorial(n - 1); // jika bukan nol, rekursif.
      // dan mengembalikan nilai faktorial dari n.
  }
}
void *calculate_factorials(void *arg)
/*
Digunakan sebagai fungsi thread.
Fungsi ini menerima argumen berupa pointer ke array barisKolom
yang berisi indeks baris dan kolom dari elemen matriks yang a
kan dihitung faktorialnya.
*/
{
  int *barisKolom = (int*)arg;
  // Pointer arg dan menyimpannya ke dalam variabel i dan j.
  unsigned long long i = barisKolom[0];
  unsigned long long j = barisKolom[1];
  /*
  Program kemudian membuat 20 thread yang akan menjalankan fungsi
  factorial_thread dengan masing-masing thread bertanggung jawab
  menghitung faktorial dari satu elemen matriks.
  Indeks baris dan kolom dari setiap elemen matriks
  dikirim sebagai argumen ke fungsi factorial_thread.
  */
  unsigned long long hasil = factorial(temp[i * 5 + j]); // pamggil fungsi factorial
  // panggil fungsi factorial
  temp[i * 5 + j] = hasil;
  pthread_exit(NULL);
  // fungsi akan memanggil pthread_exit() untuk keluar dari thread
}
int main()
{
  clock_t start = clock();
  // untuk memulai perhitungan waktu, yang disimpan kesalam variabel start.
  unsigned long long shmid, *hasil;
   /*
  menggunakan unsigned long long untuk menyamakan variabel generate angka random matrix.
  *hasil = digunakan sebagai pointer yang menunjuk ke lokasi memori yang sama dengan shared memory yang digunakan.
  shmid = untuk menyimpan id shared memory yang digunakan.
  */
  key_t key = 1234; // untuk mengidentifikasi shared memory.
  shmid = shmget(key, sizeof(unsigned long long[4][5]), 0666);
  /*
  program membuat shared memory dengan menggunakan fungsi shmget().
  Fungsi ini membutuhkan tiga parameter, yaitu key, ukuran shared memory dalam byte, dan flag
  yang digunakan untuk mengontrol hak akses shared memory.


  Disini ukuran shared memory yang dibuat sesuai dengan ukuran matriks hasil perkalian, yaitu 4x5.
  Flag yang digunakan adalah 0666,
  yang mengindikasikan untuk membuat shared memory.
  */
  hasil = shmat(shmid, NULL, 0); // untuk meng-attach shared memory ke variabel hasil.


  printf("Berikut adalah Matriks Awal di dalam file 'Kalian':\n");
  for(unsigned long long i = 0; i < 4; i++)
  // digunakan untuk mengiterasi baris matriks..
  {
      for(unsigned long long j = 0; j < 5; j++)
      // digunakan untuk mengiterasi kolom matriks.
      {
          printf("%lld ", hasil[i * 5 + j]);
          // digunakan untuk menampilkan elemen matriks pada baris i dan kolom j.
      }
      printf("\n");
  }


  temp = (unsigned long long *) malloc(sizeof(unsigned long long[4][5]));
  // malloc = mengalokasikan memori dinamin ukuran 4 x 5.
  if (temp == NULL) { // cek apalah alokasi memori berhasil atau tidak.
  // kalau temp = null, keluar.
      perror("malloc");
      exit(1); // keluar
  }
  for(unsigned long long i = 0; i < 4; i++)
  // digunakan untuk mengiterasi baris matriks.
  {
      for(unsigned long long j = 0; j < 5; j++)
      // digunakan untuk mengiterasi kolom matriks.
      {
          temp[i * 5 + j] = hasil[i * 5 + j]; // memberikan nilai hasil ke temp.
          // seluruh hasil dari variabel hasil dicopy ke variabel temp.
      }
  }
  pthread_t threads[20]; // deklarasikan array thread
  // ukuran 20
  int index[20][2];
  // deklarasi index yang berisi indeks baris dan kolom elemen matriks.
  int count = 0; // hitung jumlah thread.


  printf("\nBerikut adalah hasil faktorial dari Matrix awal:\n");
  for(unsigned long long i = 0; i < 4; i++)
  // digunakan untuk mengiterasi baris matriks
  // yang mana matriks memiliki 4 baris
  {
      for(unsigned long long j = 0; j < 5; j++)
      // digunakan untuk mengiterasi kolom matriks.
      // yang mamna matriks memiliki 5 kolom.
      {
          index[count][0] = i;
          // Menetapkan nilai indeks baris i dari matriks ke indeks
          // pertama dari array 2 dimensi 'index' dengan indeks ke-'count'.
          index[count][1] = j;
          // Menetapkan nilai indeks kolom j dari matriks ke indeks
          // kedua dari array 2 dimensi 'index' dengan indeks ke-'count'.
          pthread_create(&threads[count], NULL, calculate_factorials, (void *)&index[count]);
          /*
          Membuat thread baru dan menetapkan fungsinya ke 'calculate_factorials'.
          Menetapkan argumen untuk fungsi tersebut dengan alamat dari array 2
          dimensi 'index' pada indeks ke-'count'.
          Menyimpan ID thread baru ke dalam array.
          */
          count++; // tambah 1 hitungan thread
      }
  }
  for(int i = 0; i < count; i++)
  // Mengulang iterasi untuk setiap thread yang dibuat, mulai dari 0 hingga 'count' - 1.
  {
      pthread_join(threads[i], NULL);// Menunggu thread sesuai selesai dieksekusi.
      // Menunggu thread dengan ID yang sesuai selesai dieksekusi.
  }


  for(unsigned long long i = 0; i < 4; i++)
  // Mengulang iterasi untuk indeks baris i dalam matriks, mulai dari 0 hingga 3.
  // yang mana matriks memiliki 4 baris
  {
      for(unsigned long long j = 0; j < 5; j++)
      // Mengulang iterasi untuk indeks kolom j dalam matriks, mulai dari 0 hingga 4.
      // yang mamna matriks memiliki 5 kolom.
      {
          printf("%lld ", temp[i * 5 + j]);
          // Menampilkan nilai elemen matriks pada baris i dan kolom j
          // dengan memperoleh nilai dari array 'temp' pada indeks yang sesuai.
      }
      printf("\n"); // spasi
  }
  shmdt(hasil); // Mengeksekusi operasi shmdt pada shared memory 'hasil',
  // yang menunjukkan bahwa memori tersebut tidak lagi digunakan oleh program. // shmctl(shmid, IPC_RMID, NULL);
  clock_t end = clock(); // menghentikan perhitungan waktu
  // dan menyimpannya di dalam variabel end
  printf("\nWaktu yang dibutuhkan adalah %lf detik.\n", (double) (end - start) /CLOCKS_PER_SEC);
  // menampilkan waktu pengerjaan program
  return 0;
}
