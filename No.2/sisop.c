#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <time.h>


unsigned long long *temp; // variabel hasil nantinya digunakan untuk menyimpan hasil operasi faktorial dari setiap elemen matriks.
// Variabel ini akan digunakan untuk menampung nilai matriks setelah diambil dari shared memory.
unsigned long long factorial(unsigned long long n)
// fungsi menghitung faktorial dari sebuah bilangan n.
{
  if(n == 0) return 1; // jika n = 0(faktorial dari 0), hasilnya return 1
  else
  {
      return n * factorial(n - 1); // jika bukan nol, rekursif.
      // dan mengembalikan nilai faktorial dari n.
  }
}


int main()
{    
    clock_t start = clock();
    // untuk memulai perhitungan waktu, yang disimpan kesalam variabel start.
    unsigned long long shmid, *hasil;
    /*
    menggunakan unsigned long long untuk menyamakan variabel generate angka random matrix.
    *hasil = digunakan sebagai pointer yang menunjuk ke lokasi memori yang sama dengan shared memory yang digunakan.
    shmid = untuk menyimpan id shared memory yang digunakan.
    */
    key_t key = 1234; // untuk mengidentifikasi shared memory.


    shmid = shmget(key, sizeof(unsigned long long[4][5]), 0666);
    /*
    program membuat shared memory dengan menggunakan fungsi shmget().
    Fungsi ini membutuhkan tiga parameter, yaitu key, ukuran shared memory dalam byte, dan flag
    yang digunakan untuk mengontrol hak akses shared memory.


    Disini ukuran shared memory yang dibuat sesuai dengan ukuran matriks hasil perkalian, yaitu 4x5.
    Flag yang digunakan adalah 0666,
    yang mengindikasikan untuk membuat shared memory.
    */
    hasil = shmat(shmid, NULL, 0);
   // fungsi shmat() berfungsi untuk meng-attach shared memory ke variabel result.




  printf("Berikut adalah Matriks Awal di dalam file 'Kalian':\n");
    for(unsigned long long i = 0; i < 4; i++)
      // digunakan untuk mengiterasi baris matriks.
    {
        for(unsigned long long j = 0; j < 5; j++)
        // digunakan untuk mengiterasi kolom matriks.
        {
            printf("%lld ", hasil[i * 5 + j]);
            // digunakan untuk menampilkan elemen matriks pada baris i dan kolom j.
        }
        printf("\n"); // spasi
    }


    temp = (unsigned long long *) malloc(sizeof(unsigned long long[4][5])); // alokasikan memori dinamin ukuran 4 x 5.
    // malloc = mengalokasikan memori dinamin ukuran 4 x 5.


    if (temp == NULL) { // cek apalah alokasi memori berhasil atau tidak.
  // kalau temp = null, keluar.
        perror("malloc");
        exit(1);// keluar
    }
    for(unsigned long long i = 0; i < 4; i++)
    // digunakan untuk mengiterasi baris matriks.
    {
        for(unsigned long long j = 0; j < 5; j++)
        // digunakan untuk mengiterasi kolom matriks.
        {
            temp[i * 5 + j] = hasil[i * 5 + j]; // memberikan nilai hasil ke temp.
            // seluruh hasil dari variabel hasil dicopy ke variabel temp.
        }
    }
    printf("\nBerikut adalah hasil faktorial dari Matrix awal:\n");
    for(unsigned long long i = 0; i < 4; i++)
    // digunakan untuk mengiterasi baris matriks
    // yang mana matriks memiliki 4 baris
    {
        for(unsigned long long j = 0; j < 5; j++)
        // digunakan untuk mengiterasi kolom matriks.
        // yang mamna matriks memiliki 5 kolom.
        {
            printf("%lld ", factorial(temp[i * 5 + j])); //  Menampilkan nilai pada array hasil.
            //  Menampilkan nilai pada indeks i*5+j pada array hasil.
        }
        printf("\n");// spasi
    }


    shmdt(hasil); // Mengeksekusi operasi shmdt pada shared memory 'hasil',
    // yang menunjukkan bahwa memori tersebut tidak lagi digunakan oleh program.
    shmctl(shmid, IPC_RMID, NULL); // shmctl() adalah fungsi yang digunakan untuk melakukan kontrol terhadap shared memory segment yang telah dibuat.
    /*
    shmctl(shmid, IPC_RMID, NULL) digunakan untuk menghapus shared memory segment
    yang telah dibuat dengan shmget().
   
    IPC_RMID adalah flag yang menandakan bahwa shared memory segment yang
    ditunjukkan oleh shmid harus dihapus.
   
    NULL digunakan untuk memberikan opsi tambahan pada penghapusan segment,
    namun pada kasus ini tidak diperlukan.


    Setelah fungsi ini dijalankan, shared memory segment akan terhapus dari sistem.
    */
    clock_t end = clock(); // menghentikan perhitungan waktu
  // dan menyimpannya di dalam variabel end


    printf("\nWaktu yang dibutuhkan adalah %lf detik\n", (double) (end - start)/CLOCKS_PER_SEC);
    // print waktu yang dibutuhkan program


    return 0;
}
