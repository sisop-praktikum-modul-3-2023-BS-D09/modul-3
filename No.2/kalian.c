#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/ipc.h> // deklarasi fungsi dan struktur yang digunakan untuk mengakses sistem antar-proses.
#include <sys/shm.h> // deklarasi fungsi dan struktur yang digunakan untuk mengelola shared memory.
#include <stdlib.h>
#include <time.h> // deklarasi fungsi-fungsi untuk melakukan manipulasi waktu.
int main()
{
   unsigned long long *hasil, shmid;
   /*
   menggunakan unsigned long long untuk menyamakan variabel generate angka random matrix.

   *result = digunakan sebagai pointer yang menunjuk ke lokasi memori yang sama dengan shared memory yang digunakan.
   shmid = untuk menyimpan id shared memory yang digunakan.
   */
   key_t key = 1234; // untuk mengidentifikasi shared memory.
   unsigned long long matrix1[4][2]; // matriks pertama dengan ukuran 4x2.
   unsigned long long matrix2[2][5]; // matriks kedua dengan ukuran 2x5.
   srand(time(0)); // mengacak nilai.
   /*
   srand() untuk mengambil parameter waktu saat itu .
   (time(0)) untuk mengacak nilai.
   */
   shmid = shmget(key, sizeof(unsigned long long [4][5]), IPC_CREAT | 0666); // 0666, yang mengindikasikan shared memory dibuat jika belum ada, dan dapat dibaca dan ditulis oleh semua pengguna.
   /*
   program membuat shared memory dengan menggunakan fungsi shmget().
   Fungsi ini membutuhkan tiga parameter, yaitu key, ukuran shared memory dalam byte, dan flag
   yang digunakan untuk mengontrol hak akses shared memory.

   Disini ukuran shared memory yang dibuat sesuai dengan ukuran matriks hasil perkalian, yaitu 4x5.
   Flag yang digunakan adalah IPC_CREAT | 0666,
   yang mengindikasikan shared memory dibuat jika belum ada,
   dan dapat dibaca dan ditulis oleh semua pengguna.
   */
   hasil = shmat(shmid, NULL, 0);
   // fungsi shmat() berfungsi untuk meng-attach shared memory ke variabel result.

   printf("Generate Matriks 1:\n");
   for(unsigned long long i = 0; i < 4; i++)
      // menggunakan unsigned long long untuk menyamakan variabel generate angka random matrix.
      // dinyatakan i(baris) hanya boleh sampai 4, maka loop 4x
   {
       for(unsigned long long j = 0; j < 2; j++)
        // dinyatakan j(kolom) hanya boleh sampai 2, maka loop 2x
       {
           matrix1[i][j] = (rand() % 5) + 1;
           /*
           menggunakan fungsi random yang akan meng-generate angka acak
           yang mana angka random tersebut akan di mod 5 +1, untuk mengambil angka
           yang HANYA diantara 1 hingga 5
           */
           printf("%lld ", matrix1[i][j]);
           /*
           Untuk menampilkan nilai elemen matriks pada baris ke-i dan kolom ke-j,
           yang disimpan dalam variabel matrix1. %lld digunakan sebagai specifier untuk tipe data long long.
           */
       }
       printf("\n"); // spasi
   }
   printf("\nGenerate Matriks 2:\n");
   for(unsigned long long i = 0; i < 2; i++)
    // menggunakan unsigned long long untuk menyamakan variabel generate angka random matrix.
    // dinyatakan i(baris) hanya boleh sampai 2, maka loop 2x
   {
       for(unsigned long long j = 0; j < 5; j++)
        // dinyatakan j(kolom) hanya boleh sampai 5, maka loop 5x
       {
           matrix2[i][j] = (rand() % 4) + 1;
           /*
           menggunakan fungsi random yang akan meng-generate angka acak
           yang mana angka random tersebut akan di mod 4 +1, untuk mengambil angka
           yang HANYA diantara 1 hingga 4
           */
           printf("%lld ", matrix2[i][j]);
           /*
           Untuk menampilkan nilai elemen matriks pada baris ke-i dan kolom ke-j,
           yang disimpan dalam variabel matrix2. %lld digunakan sebagai specifier untuk tipe data long long.
           */
       }
       printf("\n"); // spasi
   }
   for(unsigned long long i = 0; i < 4; i++)
    // untuk mengalikan.
    // menggunakan unsigned long long untuk menyamakan variabel generate angka random matrix.
    // loop 4x untuk baris dikarenakan perkalian matrix.
   {
       for(unsigned long long j = 0; j < 5; j++)
        // menggunakan unsigned long long untuk menyamakan variabel generate angka random matrix.
        // loop 4x untuk baris dikarenakan perkalian matrix.
       {
           hasil[i * 5 + j] = 0;
           for(unsigned long long x = 0; x < 2; x++)// pengalian matrix.
           {
               hasil[i * 5 + j] += matrix1[i][x] * matrix2[x][j];
               // menyimpan hasil perkalian matrix di variabel hasil.
           }
       }
   }
   printf("\nHasil Perkalian Matriks:\n");
   for(unsigned long long i = 0; i < 4; i++)
    // menggunakan unsigned long long untuk menyamakan variabel generate angka random matrix.
    // loop 4x karena hasil perkalian matrix ini adalah 4 baris.
   {
       for(unsigned long long j = 0; j < 5; j++)
        // menggunakan unsigned long long untuk menyamakan variabel generate angka random matrix.
        // loop 5x karena hasil perkalian matrix ini adalah 5 kolom.
       {
           printf("%lld ", hasil[i * 5 + j]); // print hasil matrix.
       }
       printf("\n"); // spasi
   }
   shmdt(hasil);
   /*
   shmdt() pada program yang menggunakan Shared Memory
   digunakan untuk melepaskan/merelase alamat memori yang telah dialokasikan
   untuk segment shared memory, dalam hal ini fungsi tersebut melepaskan alamar memori hasil
   untuk shared memory.
   */
   return 0; // menggembalikan nilai 0.
}







