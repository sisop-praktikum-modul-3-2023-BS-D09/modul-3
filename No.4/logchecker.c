#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <pthread.h>

#define MAX_LINE_LENGTH 300

char dir[MAX_LINE_LENGTH][MAX_LINE_LENGTH];
int dirCount[MAX_LINE_LENGTH];
char eks[MAX_LINE_LENGTH][MAX_LINE_LENGTH];
int eksCount[MAX_LINE_LENGTH];
int indexDir = 0;
int indexEks = 0;

int count_accessed(){
    // untuk menghitung berapa banyak file yang diakses 
    // berdasarkan isi dari file log.txt.
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];
    int counter = 0;
    strcpy(nama_file, "log.txt");
    textfile = fopen(nama_file, "r"); // // menggunakan fopen() dan membaca isi file tersebut menggunakan fgets()   
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        if (strstr(line, "ACCESSED") != NULL) {
            // mencari kata kunci "ACCESSED".
            counter++;
            // Jika kata kunci tersebut ditemukan dalam baris yang dibaca,
            //  maka variabel counter akan ditambahkan 1.

        }

    }
    fclose(textfile);
    return counter;
}

int check_dir(char dirs[] ){
    // Fungsi check_dir akan melakukan pencarian pada array dir
    for(int i=0; i<indexDir;i++){
        if(strcmp(dirs, dir[i]) == 0)
        // jika ditemukan elemen pada array yang sama dengan dirs
            return i;
            // maka akan mengembalikan indeks elemen tersebut di dalam array.
    }
    return -1;
    // Namun jika tidak ditemukan, maka fungsi akan mengembalikan nilai -1.
}

int check_eks(char ekss[] ){
    // mencari elemen array yang sama dengan ekss
    for(int i=0; i<indexDir;i++){
        if(strcmp(ekss, eks[i]) == 0)
            return i;// dan mengembalikan indeksnya jika ditemukan
    }
    return -1;// dan mengembalikan nilai -1 jika tidak.
}

void all_dir_count(){

    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];

    strcpy(nama_file, "log.txt");
    textfile = fopen(nama_file, "r");     
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        if (strstr(line, "MADE ") != NULL) {
            line[strlen(line)-1] = '\0';
            char *pos = strstr(line, "MADE ");
            char *file = pos + strlen("MADE ");

            strcpy(dir[indexDir], file);
            dirCount[indexDir] = 0;
            indexDir++;
        }
    }
    fclose(textfile);

    strcpy(nama_file, "log.txt");
    textfile = fopen(nama_file, "r"); 
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        if (strstr(line, "> ") != NULL) {
                line[strlen(line)-1] = '\0';
                char *pos = strstr(line, "> ");
                char *file = pos + strlen("> ");

                int num = check_dir(file);
                dirCount[num]++;
        }
    }
    fclose(textfile);


    for(int i=0;i<indexDir; i++){
        for(int j=0; j<indexDir-1;j++){
            if(dirCount[j] > dirCount[j+1]){
                int temp;
                temp = dirCount[j];
                dirCount[j] = dirCount[j+1];
                dirCount[j+1] = temp;

                char buffer[MAX_LINE_LENGTH];
                strcpy(buffer, dir[j]);
                strcpy(dir[j], dir[j+1]);
                strcpy(dir[j+1], buffer);

            }
        }
    }
    
    for(int i=0; i<indexDir; i++){
        printf("%s : %d\n", dir[i], dirCount[i]);
    }
    
}

void all_eks_count(){
    // untuk menghitung jumlah file untuk setiap ekstensi file yang ada
    // pada sebuah direktori dan menampilkannya ke layar.
    FILE *textfile; // sebagai pointer ke file.
    char line[MAX_LINE_LENGTH];
    // sebagai variabel untuk menyimpan baris yang dibaca dari file
    char nama_file[MAX_LINE_LENGTH];
    // sebagai nama file yang akan dibaca.

    strcpy(nama_file, "extensions.txt");
    textfile = fopen(nama_file, "r");
    // membuka file yang bernama extensions.txt dengan mode "r"
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        // membaca isi file baris per baris menggunakan fungsi
        line[strlen(line)-2] = '\0';
        // menghapus karakter "\n" pada setiap baris yang dibaca dari file
        // menggantinya dengan karakter nul '\0'
        // sehingga tidak mengganggu saat mengecek ekstensi file pada program selanjutnya.
        strcpy(eks[indexEks], line); // disimpan ke dalam array eks
        eksCount[indexEks] = 0;// jumlah file untuk setiap ekstensi diset menjadi 0 pada array eksCount.
        indexEks++;
    }
    fclose(textfile);
    strcpy(eks[indexEks], "other");
    eksCount[indexEks]=0;
    indexEks++;

    strcpy(nama_file, "log.txt");
    textfile = fopen(nama_file, "r"); 
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        // mengecek setiap baris pada file.
        if (strstr(line, "MOVED ") != NULL) {
            line[strlen(line)-1] = '\0';
            char *start = strstr(line, "MOVED ");
            // mencari kata "MOVED " pada setiap baris yang dibaca dari file log.
            char *en = strstr(start, " file :");
            //  program akan mencari kata " file :" pada baris yang sama
            char file[MAX_LINE_LENGTH];
            strncpy(file, start + strlen("MOVED "), en - start - strlen("MOVED ")); //copy
            file[en - start - strlen("MOVED ")] = '\0';
            // ditambahkan pada akhir variabel file agar tidak terjadi error pada program selanjutnya.
            int num = check_eks(file);
            // mengecek apakah ekstensi file yang terdapat pada variabel file
            // sudah ada pada array eks atau belum menggunakan fungsi check_eks().
            eksCount[num]++;
        }
    }
    fclose(textfile);// file tersebut akan ditutup

    // mengurutkan elemen-elemen dalam sebuah array.
    for(int i=0;i<indexEks; i++){
        // untuk menelusuri seluruh elemen dalam array eksCount
        for(int j=0; j<indexEks-1;j++){
            //  untuk membandingkan nilai elemen pada indeks ke-j dan indeks ke-j+1
            if(eksCount[j] > eksCount[j+1]){
                // memeriksa apakah eksCount[j] > eksCount[j+1]
                int temp;
                temp = eksCount[j];
                eksCount[j] = eksCount[j+1];
                eksCount[j+1] = temp;
                // maka nilai eksCount[j] dan eksCount[j+1] ditukar tempat

                char buffer[MAX_LINE_LENGTH];
                strcpy(buffer, eks[j]);
                strcpy(eks[j], eks[j+1]);
                strcpy(eks[j+1], buffer);

            }
        }
    }
    
    for(int i=0; i<indexEks; i++){
        printf("%s : %d\n", eks[i], eksCount[i]);
    }
}

int main(){
    printf("ACESSED COUNT : %d\n\n", count_accessed());
    // untuk menghitung berapa banyak file yang telah diakses
    // di dalam sistem, dan mencetak nilai tersebut ke layar menggunakan printf().

    printf("DIRECTORY COUNT : \n");
    all_dir_count();
    // menghitung berapa banyak file yang ada di setiap direktori di dalam sistem.

    printf("\nEKSTENSION COUNT : \n");
    all_eks_count();
    // menghitung jumlah file dengan setiap ekstensi file yang ada di dalam sistem.
}