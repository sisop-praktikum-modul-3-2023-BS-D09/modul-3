#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <stdio.h>

pid_t child;
int status;

int main(){

    char url[] = "https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download";
    char file[] = "hehe.zip";

    child = fork();
    if(child == 0) {
        execlp("wget", "wget", "-O", file, url, NULL);
        /*
        char *argv[] = untuk mendeklarasikan sebuah array string untuk menyimpan 
                        argumen baris perintah yang akan diteruskan ke fungsi execv().

        wget = sebuah program yang digunakan untuk mengunduh file dari internet.

        -0 = opsi untuk menentukan nama file output dari unduhan yang dilakukan.

        execv = untuk menggantikan image program saat ini dengan
                image program baru yang disebut dalam argumen pertama tadi.

        & = untuk menjalankan command wget dalam background, sehingga child
            process dapat berlanjut tanpa menunggu operasi download selesai
        */
    }else if(child < 0){
    exit(EXIT_FAILURE);
    } 

    while ((wait(&status)) > 0);
    child = fork();
    if(child == 0) {//child yang akan dijalankan
        execlp("unzip", "unzip", file, NULL);
        /*
        execv() mengeksekusi program zip dan melakukan kompresi
        pada direktori yang sesuai dengan argumen yang diberikan
        */
    }else if(child < 0){
    exit(EXIT_FAILURE);
    } 

    while ((wait(&status)) > 0);
    child = fork();
    if(child == 0) { //child yang akan dijalankan
        execlp("rm", "rm", file, NULL);
        /*
        execv() mengeksekusi program zip dan melakukan kompresi
        pada direktori yang sesuai dengan argumen yang diberikan
        */

    }else if(child < 0){
    exit(EXIT_FAILURE);
    } 
    printf("Sudah download");

}
