#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <stdio.h>
#include <dirent.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <pthread.h>


/*
Program ini adalah sebuah program yang dapat melakukan kategorisasi
file dalam suatu direktori dengan cara memindahkan file-file tersebut
ke dalam direktori yang sesuai dengan ekstensi file.


Program ini juga mencatat setiap aktivitas yang dilakukan,
seperti akses, pemindahan, dan pembuatan direktori ke dalam file log.txt.
*/
#define MAX_LINE_LENGTH 300
// konstanta yang digunakan untuk menyimpan panjang maksimum dari setiap string yang digunakan pada program ini.


int extCount = 0; // variabel bertipe integer yang digunakan untuk menyimpan jumlah ekstensi file yang sudah ditemukan.
char ekstension[MAX_LINE_LENGTH][10]; // array 2 dimensi bertipe char yang digunakan untuk menyimpan daftar ekstensi file yang sudah ditemukan.
int ekstensionDir[MAX_LINE_LENGTH];  //  array bertipe integer yang digunakan untuk menyimpan status direktori yang sudah dibuat untuk setiap ekstensi file yang ditemukan.
char buffer[MAX_LINE_LENGTH]; // array bertipe char yang digunakan untuk menyimpan informasi yang akan dicatat ke dalam file log.txt.
int max = 0; // variabel bertipe integer yang digunakan untuk menyimpan ukuran file terbesar yang ditemukan.
pthread_t threads[2]; // array bertipe pthread_t yang digunakan untuk menyimpan ID thread yang dibuat pada program ini.


int mode = 1; // variabel bertipe integer yang digunakan untuk menyimpan mode logging yang akan dilakukan.
// Mode ini dapat bernilai 1 (accessed), 2 (moved), atau 3 (made).
char param1[MAX_LINE_LENGTH];
char param2[MAX_LINE_LENGTH];
char param3[MAX_LINE_LENGTH];
// variabel bertipe char yang digunakan untuk menyimpan parameter yang dibutuhkan untuk mencatat log.


void *logging(){
    /*
    Program tersebut adalah sebuah fungsi bernama "logging"
    yang digunakan untuk mencatat aktivitas pada direktori dan file dalam file log.txt.
   
    Fungsi ini menerima beberapa argumen seperti mode, param1, param2, dan param3
    yang digunakan untuk mencatat informasi terkait aktivitas yang dilakukan. Informasi yang dicatat meliputi waktu akses/move/made, jenis aktivitas, dan lokasi akses/move/made.
    */
    char file_log[] = "log.txt"; // digunakan untuk mencatat aktivitas.


    time_t now = time(NULL); // untuk menyimpan waktu saat ini dalam format yang diinginkan.
    struct tm *t = localtime(&now); // strftime() untuk mengubah waktu saat ini menjadi string dengan format tertentu. String tersebut akan digunakan sebagai bagian dari pesan log yang akan dicatat.
    char datetime[20];
    strftime(datetime, sizeof(datetime), "%d-%m-%Y %H:%M:%S", t);


    // Cek mode yang diberikan sebagai argumen pada fungsi logging. Terdapat tiga mode yaitu 1 (accessed), 2 (moved), dan 3 (made).
    if(mode == 1){
        char mode[] = "ACCESSED"; // membuat sebuah string "mode" dengan isi "ACCESSED"
        char write[MAX_LINE_LENGTH]; // Lalu membuat sebuah string "write" yang berisi pesan log yang akan dicatat
        sprintf(write, "%s %s %s", datetime, mode, param1);


        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
        /*
        String "write" berisi waktu akses, jenis aktivitas, dan lokasi akses.
        Pesan log tersebut akan dicatat ke dalam file log.txt
        dengan menggunakan perintah "system(cmd)" dimana "cmd" berisi perintah
        untuk menulis pesan log tersebut ke dalam file log.txt.
        */
    }else if(mode == 2){
        char mode[] = "MOVED"; // membuat sebuah string "mode" dengan isi "MOVED"
        char write[MAX_LINE_LENGTH]; // Lalu membuat sebuah string "write" yang berisi pesan log yang akan dicatat
        sprintf(write, "%s %s %s file : %s > %s", datetime, mode, param1, param2, param3);


        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
        /*
        String "write" berisi waktu akses, jenis aktivitas, dan lokasi akses.
        Pesan log tersebut akan dicatat ke dalam file log.txt
        dengan menggunakan perintah "system(cmd)" dimana "cmd" berisi perintah
        untuk menulis pesan log tersebut ke dalam file log.txt.
        */
    }else{
        char mode[] = "MADE"; // membuat sebuah string "mode" dengan isi "MADE"
        char write[MAX_LINE_LENGTH]; // Lalu membuat sebuah string "write" yang berisi pesan log yang akan dicatat
        sprintf(write, "%s %s %s", datetime, mode, param1);


        char cmd[MAX_LINE_LENGTH];
        sprintf(cmd, "echo '%s' >> %s", write, file_log);
        system(cmd);
        /*
        String "write" berisi waktu akses, jenis aktivitas, dan lokasi akses.
        Pesan log tersebut akan dicatat ke dalam file log.txt
        dengan menggunakan perintah "system(cmd)" dimana "cmd" berisi perintah
        untuk menulis pesan log tersebut ke dalam file log.txt.
        */
    }
}


void create_thread_logging(int modeNew, char param1New[], char param2New[], char param3New[]){
    // Untuk membuat thread yang menjalankan fungsi logging().
    mode = modeNew;
    // set variabel mode, param1, param2, dan param3 sesuai dengan nilai argumen yang diberikan.
    strcpy(param1, param1New);
    strcpy(param2, param2New);
    strcpy(param3, param3New);


    pthread_join(threads[1], NULL); // ungsi akan memanggil pthread_join() pada threads[1] untuk memastikan bahwa thread sebelumnya sudah selesai dijalankan.
    pthread_create(&threads[1], NULL, logging, NULL); // Setelah itu, fungsi akan membuat thread baru dengan memanggil pthread_create()
    pthread_join(threads[1], NULL); // memanggil pthread_join() lagi pada threads[1] untuk memastikan bahwa thread baru sudah selesai dijalankan.
}


const char *get_filename_ext(char *filename) {
    // untuk mendapatkan ekstensi dari sebuah file yang diberikan.
    // Fungsi ini menerima argumen filename yang merupakan nama file.
    const char *dot = strrchr(filename, '.');
    // menggunakan strrchr untuk mencari titik terakhir pada string filename, yang menandakan awal dari ekstensi file.
    if(!dot || dot == filename) return "";
    // Jika tidak ditemukan titik atau titik berada pada awal string, maka fungsi mengembalikan string kosong.
    return dot + 1;
    // return dot + 1 akan mengembalikan pointer ke karakter 't', sehingga fungsi akan mengembalikan string "txt".
}


int check_ekstension(char eks[]){
    // Fungsi check_ekstension() digunakan untuk memeriksa apakah sebuah ekstensi file sudah pernah ditemukan sebelumnya. Fungsi ini menerima argumen eks yang merupakan ekstensi file yang akan diperiksa.
    for(int i=0; i<extCount;i++){
        // memeriksa apakah ekstensi yang diberikan sama dengan elemen array yang sedang diiterasi menggunakan fungsi strcmp()
        if(strcmp(eks, ekstension[i]) == 0)
        // Jika ditemukan elemen array yang sama dengan ekstensi yang diberikan, maka fungsi akan mengembalikan indeks elemen array tersebut.
            return i;
    }
    return -1;
    // Jika tidak ditemukan, fungsi akan mengembalikan nilai -1
}


void *createdir(){
    // Fungsi createdir() digunakan untuk membuat direktori categorized dan beberapa direktori lainnya di dalamnya berdasarkan daftar ekstensi file yang terdapat dalam file extensions.txt. Fungsi ini juga mencatat aktivitas pembuatan direktori dalam file log.txt.
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];


    strcpy(folder_name, "./categorized"); // membuka file categorized
    // menginisialisasi variabel folder_name dengan string "./categorized" yang akan digunakan sebagai nama folder utama.
    sprintf(cmd, "mkdir -p %s", folder_name);
    // mkdir dengan argumen "-p" untuk membuat folder tersebut jika belum ada.
    system(cmd);


    create_thread_logging( 3, folder_name, "null", "null");
    // mencatat aktivitas pembuatan direktori dalam file "log.txt".


    strcpy(nama_file, "extensions.txt"); // // membuka file extensions
    int index=0;
    textfile = fopen(nama_file, "r");


    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        line[strlen(line)-2] = '\0';
        // menghapus karakter newline pada akhir baris dengan line[strlen(line)-2] = '\0'.
        sprintf(cmd, "mkdir -p %s/%s", folder_name, line);
        // membuat folder tersebut jika belum ada, dan menyimpan nama ekstensi ke dalam array ekstension.
        strcpy(ekstension[index], line);
        ekstensionDir[index] = 0;
        index++;
        extCount++;
        system(cmd);


        sprintf(buffer, "%s/%s", folder_name, line);
        create_thread_logging(3, buffer, "null", "null");
        // mode "3" dan argumen buffer yang berisi nama folder untuk setiap ekstensi yang baru dibuat.
    }
    fclose(textfile); // tutup extensions.txt
    sprintf(cmd, "mkdir -p %s/other", folder_name);
    system(cmd);


    sprintf(buffer, "%s/other", folder_name);
    create_thread_logging( 3, buffer, "null", "null");
}


void listdir(const char *name)
// Fungsi listdir() digunakan untuk mencatat daftar file dalam direktori dan subdirektori tertentu dalam file listFiles.txt.
// Fungsi ini menerima argumen name yang merupakan nama direktori.
{
    DIR *dir;
    struct dirent *entry;
    char cmd[MAX_LINE_LENGTH];


    if (!(dir = opendir(name)))
    // membuka direktori yang ditentukan menggunakan fungsi opendir()
        return;


    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR) { // Jika entry yang dibaca adalah sebuah direktori
            char path[1024];
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            snprintf(path, sizeof(path), "%s/%s", name, entry->d_name);
            create_thread_logging(1, path, "null", "null");
            listdir(path); // maka fungsi akan melakukan rekursi untuk mengecek
            // daftar file dalam direktori tersebut dengan memanggil kembali fungsi listdir()
        } else { // Jika entry yang dibaca adalah sebuah file
            sprintf(cmd, "echo '%s/%s' >> listFiles.txt", name,entry->d_name);
            // mencatat path file tersebut ke dalam file listFiles.txt menggunakan perintah echo dan system()
            system(cmd);
        }
    }
    closedir(dir);
}




void *listfile(){
    // Fungsi listfile() digunakan untuk menjalankan fungsi listdir() pada direktori ./files. Fungsi ini juga membaca nilai maksimum yang diperbolehkan untuk jumlah file dalam direktori dan subdirektori tertentu dari file max.txt.
    char charMax [3];
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];


    strcpy(nama_file, "max.txt"); // membaca file max.txt yang berisi nilai maksimum yang
    // diperbolehkan untuk jumlah file dalam direktori dan subdirektori tertentu.
    textfile = fopen (nama_file, "r"); fgets (charMax, 3, textfile); fclose(textfile);
    max = atoi(charMax);


    listdir("./files"); // untuk mencatat daftar file dalam direktori dan subdirektori tertentu dalam file listFiles.txt
}


void *movefile(){
    // Fungsi movefile() digunakan untuk memindahkan file-file dalam direktori dan subdirektori tertentu ke dalam direktori sesuai dengan ekstensinya. Pemindahan dilakukan berdasarkan daftar file yang terdapat dalam file listFiles.txt. Fungsi ini juga mencatat aktivitas pemindahan file dalam file log.txt.
    char folder_name[MAX_LINE_LENGTH];
    char cmd[MAX_LINE_LENGTH];
    FILE *textfile;
    char line[MAX_LINE_LENGTH];
    char nama_file[MAX_LINE_LENGTH];


    strcpy(nama_file, "listFiles.txt");
    // membuka file listFiles.txt dan membacanya secara baris per baris menggunakan fungsi fgets().
    textfile = fopen(nama_file, "r");    
    while(fgets(line, MAX_LINE_LENGTH, textfile)){
        line[strlen(line)-1] = '\0';
        char eks[MAX_LINE_LENGTH];
        sprintf(eks,"%s",get_filename_ext(line)); // untuk mendapatkan ekstensi dari file tersebut.
        for(int i = 0; eks[i]; i++){
            eks[i] = tolower(eks[i]); // ekstensi yang didapat akan diubah menjadi huruf kecil menggunakan fungsi tolower()
        }
        if(check_ekstension(eks) == -1){
            // jika ekstensi tidak terdaftar dalam daftar ekstensi yang diterima
            sprintf(cmd, "cp '%s' ./categorized/other", line);
            // maka file akan dipindahkan ke direktori "other" dalam folder categorized
            system(cmd); // menggunakan perintah "cp" dan dicatat aktivitasnya pada file log.txt menggunakan fungsi create_thread_logging().
            create_thread_logging(2, "other", line,"./categorized/other" );
        }
        else{ // Jika ekstensi terdaftar dalam daftar ekstensi yang diterima
        // program akan mengecek apakah direktori untuk ekstensi tersebut masih dapat menampung file atau tidak.
            if(ekstensionDir[check_ekstension(eks)] < max){ // ika masih dapat menampung
                sprintf(cmd, "cp '%s' ./categorized/%s", line, eks);
                // file akan dipindahkan ke direktori yang sesuai dengan ekstensinya menggunakan perintah "cp"
                system(cmd);
                ekstensionDir[check_ekstension(eks)]++;
                // dicatat aktivitasnya pada file log.txt menggunakan fungsi create_thread_logging().
                sprintf(buffer, "./categorized/%s", eks);
                create_thread_logging(2, eks, line, buffer );
            }else{ // Jika sudah tidak dapat menampung
                int num = ekstensionDir[check_ekstension(eks)] / max + 1;
                // program akan membuat direktori baru dengan nama "<ekstensi> <nomor>"
                // Setelah semua file dipindahkan, program akan selesai dan kontrol dikembalikan ke fungsi yang memanggilnya.
                sprintf(buffer, "./categorized/%s %d", eks, num);
                if(access(buffer, F_OK) != 0){
                    create_thread_logging(3,buffer, "null", "null" );
                }
                sprintf(cmd, "mkdir -p './categorized/%s %d'", eks, num);
                system(cmd);


                sprintf(cmd, "cp '%s' './categorized/%s %d'", line, eks, num);
                system(cmd);
                ekstensionDir[check_ekstension(eks)]++;


                sprintf(buffer, "./categorized/%s %d", eks, num);
                create_thread_logging(2, eks, line, buffer );
            }
        }
    }
}


void *count_eks(){
    // fungsi yang tidak mengembalikan nilai (void) dan akan dijalankan sebagai thread.
    // Fungsi ini bertujuan untuk menghitung jumlah file yang ditemukan pada setiap direktori dengan ekstensi tertentu.
    strcpy(ekstension[extCount], "other");
    // menambahkan other ke dalam daftar ekstensi (ekstension) yang ada pada program.
    // other digunakan untuk menyimpan file yang tidak memiliki ekstensi tertentu.
    DIR *directory;
    struct dirent *entry;
    directory = opendir("./categorized/other"); // direktori yang berisi file-file yang tidak memiliki ekstensi tertentu.


    create_thread_logging(1, "./categorized/other", "null", "null");


    while ((entry = readdir(directory)) != NULL) {
        if (entry->d_type == DT_REG) {
            ekstensionDir[extCount]++;
        }
    }


    closedir(directory);
    extCount++;
    // Bubble Sort
    for(int i=0;i<extCount; i++){
        for(int j=0; j<extCount-1;j++){
            if(ekstensionDir[j] > ekstensionDir[j+1]){
                int temp;
                temp = ekstensionDir[j];
                ekstensionDir[j] = ekstensionDir[j+1];
                ekstensionDir[j+1] = temp;


                strcpy(buffer, ekstension[j]);
                strcpy(ekstension[j], ekstension[j+1]);
                strcpy(ekstension[j+1], buffer);


            }
        }
    }
   
    for(int i=0; i<extCount; i++){ // menampilkan hasil
        printf("%s : %d\n", ekstension[i], ekstensionDir[i]);
    }
}


int main(){


    pthread_create(&threads[0], NULL, listfile, NULL);
    pthread_join(threads[0],NULL);
    // untuk mengambil daftar file yang ada pada direktori kerja
    // dan menyimpannya ke dalam file listFiles.txt


    pthread_create(&threads[0], NULL, createdir, NULL);
    pthread_join(threads[0],NULL);
    // untuk membuat direktori-direktori sesuai dengan
    // ekstensi file yang ditemukan pada listFiles.txt.


    pthread_create(&threads[0], NULL, movefile, NULL);
    pthread_join(threads[0],NULL);
    // untuk memindahkan file-file yang ditemukan pada listFiles.txt
    // ke direktori ekstensi yang sesuai dengan file tersebut.


    pthread_create(&threads[0], NULL, count_eks, NULL);
    pthread_join(threads[0],NULL);
    // untuk menghitung jumlah file pada setiap direktori ekstensi
    // dan menampilkan hasilnya pada layar.


    system("rm listFiles.txt"); // program akan menghapus file listFiles.txt
    system("rm -r files"); // program akan menghapus direktori files (jika ada)
    // yang terbuat selama program dijalankan.


    return 0; // dan kemudian program akan selesai.
}



