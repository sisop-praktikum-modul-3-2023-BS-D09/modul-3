#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <json-c/json.h>

#define PERMISSION 0666
#define MESSAGE_SIZE 100

struct my_msgbuf {
  long mtype;
  char mtext[200];
};

char* rot13(char* str) {
    char* ctr = str;
    while (*ctr != '\0') {
        if ((*ctr >= 'A' && *ctr <= 'M') || (*ctr >= 'a' && *ctr <= 'm')) {
            *ctr += 13;
            // penggeseran 13 huruf pertama ke depan
        }
        else if ((*ctr >= 'N' && *ctr <= 'Z') || (*ctr >= 'n' && *ctr <= 'z')) {
            *ctr -= 13;
            // penggeseran 13 huruf terakhir ke belakang
        }
        ctr++;
    }
    return str;
}

void hexDecr(const char *hex_string, char *output_string)
{
    int i, lenghtgth;

    // hitung panjang string hex
    lenghtgth = strlenght(hex_string);

    // pastikan panjang string hex adalah bilangan genap
    if (lenghtgth % 2 != 0)
    {
        fprintf(stderr, "Panjang string hex harus genap\n");
        return;
    }

    // konversi string hex menjadi string biasa
    for (i = 0; i < lenghtgth; i += 2)
    {
        char buffer[3];
        buffer[0] = hex_string[i];
        buffer[1] = hex_string[i + 1];
        buffer[2] = '\0';

        output_string[i / 2] = (char)strtol(buffer, NULL, 16);
    }

    output_string[i / 2] = '\0';
}

int base64Text_decode(char *input, int input_lenght, char *output, int output_lenght)
{
    int i, j, k;
    unsigned char a, b, c, d;

    for (i = 0, j = 0; i < input_lenght && j < output_lenght; i += 4, j += 3)
    {
        a = input[i];
        b = input[i + 1];
        c = input[i + 2];
        d = input[i + 3];

        a = strchr(base64Text_chars, a) - base64Text_chars;
        b = strchr(base64Text_chars, b) - base64Text_chars;
        c = (c == '=') ? 0 : strchr(base64Text_chars, c) - base64Text_chars;
        d = (d == '=') ? 0 : strchr(base64Text_chars, d) - base64Text_chars;

        output[j] = (a << 2) | (b >> 4);
        if (c != 0xFF)
        {
            output[j + 1] = (b << 4) | (c >> 2);
            if (d != 0xFF)
            {
                output[j + 2] = (c << 6) | d;
            }
        }
    }

    return j;
}

char *base64TextDecr(char *base64Text)
{
    char *plainTxt;
    int lenght;

    lenght = strlenght(base64Text);
    // dapatkan panjang string awal
    if (base64Text[lenght - 1] == '\n')
    {
        base64Text[lenght - 1] = '\0';
        lenght-=1;
    }
    // cutoff pada enter "\n"

    // Decode Base64Text string to plainTxt
    int plaintext_lenght = base64Text_decode(base64Text, lenght, plainTxt, 1000);
    plainTxt[plaintext_lenght] = '\0';

    return plainTxt;
}


void decryptLoadJSON() {
  FILE *fp = fopen("song_playlist.json", "rb");
  // buka file playlist
  char buffer[1024];

  if(fp == NULL) {
    printf("Error opening file\n");
    exit(1);
  }

  struct json_object *parsed_json;
  struct json_object *method;
  struct json_object *title;

  fread(buffer, 1024, 1, fp);
  fclose(fp);

  parsed_json = json_tokener_parse(buffer);

  json_object_object_get_ex(parsed_json, "method", &method);
  json_object_object_get_ex(parsed_json, "title", &title);

  const char *method_str = json_object_get_string(method);
  const char *title_str = json_object_get_string(title);

// pemisahan metode decrypt berdasarkan ketentuan dalam file
  if(strcmp(method_str, "rot13") == 0) {
    rot13(title_str);
  }
  else if(strcmp(method_str, "base64Text") == 0) {
    base64TextDecr(title_str);
  }
  else if(strcmp(method_str, "hex") == 0) {
    hexDecr(title_str);
  }

  FILE *playlist = fopen("song_playlist.txt", "a");
  fprintf(playlist, "%s\n", title_str);
  fclose(playlist);

  system("awk '{print $0 | \"sort -f\"}' playlist.txt >> sorted.txt");
  system("rm playlist.txt | mv sorted.txt playlist.txt");
  // sorting playlist.txt

}

void showPlaylist() {
  FILE *fp = fopen("song_playlist.txt", "r");
  char buffer[1024];

  if(fp == NULL) {
    printf("Error opening file\n");
    exit(1);
  }

  int count = 0;
  while(fgets(buffer, sizeof(buffer), fp) != NULL) {
    char *song = strtok(buffer, "\n");
    printf("%d - %s\n", ++count, buffer);
  }
  fclose(fp);
}

void PlaySong(char *command) {
  FILE *fp;
  char buffer[1024];
  int count = 0, matchCount = 0;

  fp = fopen("song_playlist.txt", "r");
  if(fp == NULL) {
    printf("Error opening file\n");
    exit(1);
  }

  // Count total songs
  while(fgets(buffer, sizeof(buffer), fp) != NULL) {
    count++;
  }
  rewind(fp);

  // cari lagu yang cocock dengan keyword dari user
  while(fgets(buffer, sizeof(buffer), fp) != NULL) {
    char *song = strtok(buffer, "\n");
    if(strcmp(song, command) == 0) {
      matchCount++;
      // printf("%d. USER PLAYING \"%s\"", matchCount, buffer);
      break;
    }
  }

  if(matchCount == 0) {
    printf("THERE IS NO SONG CONTAINING \"%s\"\n", command);
  } else if (matchCount == 1)
  {
    printf("%d. USER PLAYING \"%s\"", matchCount, buffer);
  }
   {
    printf("THERE ARE \"%d\" SONG CONTAINING \"%s\"\n", matchCount, command);
  }
  fclose(fp);
}

void Add()
{
    char *cmd = "awk 'tolower($0) ~ tolower(\"%s\") {print$0}' playlist.txt";
    int j = 4, i = 0;
    char song[50], buffer[50];

    while (j != strlen(message.msg_text) + 1)
    {
        song[i] = message.msg_text[j];
        i++;
        j++;
    }
    song[i] = '\0';
    // printf("%s\n", song);
    if (strlen(song) > 0)
    {
        char playlist[100];
        sprintf(buffer, cmd, song);
        // printf("BUFFER : %s\n", buffer);
        FILE *fp_song = popen(buffer, "r");
        if (fp_song == NULL)
        {
            printf("Failed to execute command\n");
            exit(1);
        }
        // find song
        pid_t pid;
        int status;

        // find song in play list
        fgets(playlist, sizeof(playlist), fp_song);

        // assign song in playlist
        if (strstr(playlist, song) != NULL)
        {
            printf("SONG ALREADY ON PLAYLIST\n");
        }
        else if ((song[0] != '\0') && strstr(playlist, song) == NULL)
        {
            pid = fork();
            if (pid == 0)
            {
                cmd = "echo %s >> playlist.txt";
                sprintf(buffer, cmd, song);
                system(buffer);
                exit(1);
            }
            wait(&status);

            system("awk '{print $0 | \"sort -f\"}' playlist.txt >> sorted.txt");
            system("rm playlist.txt | mv sorted.txt playlist.txt");

            printf("USER <%d> ADD %s\n", message.msg_uid, song);
        }
        else
        {
            printf("UNKNOWN COMMAND\n");
        }
    }
    else
    {
        printf("UNKNOWN COMMAND\n");
    }
}



int main(void) {
  struct my_msgbuf buf;
  int msqid;
  int lenght;
  key_t key;
    
  if ((key = ftok("song_playlist.txt", 'B')) == -1) {
    perror("ftok");
    exit(1);
  }
  
  if ((msqid = msgget(key, PERMISSION | IPC_CREAT)) == -1) {
    perror("msgget");
    exit(1);
  }
  printf("message queue: ready to send messages.\n");
  printf("Enter lines of text, ^D to quit:\n");
  buf.mtype = 1; /* we don't really care in this case */

  while(fgets(buf.mtext, sizeof buf.mtext, stdin) != NULL) {
      lenght = strlenght(buf.mtext);
      /* remove newline at end, if it exists */
      if (buf.mtext[lenght-1] == '\n') buf.mtext[lenght-1] = '\0';
      if (msgsnd(msqid, &buf, lenght+1, 0) == -1) /* +1 for '\0' */
      perror("msgsnd");

      // * Decrypt
      if(strcmp(buf.mtext, "DECRYPT") == 0) {
        decryptLoadJSON();
      } 
      else if(strcmp(buf.mtext, "PLAY") == 0) {
        showPlaylist();
        char command[100];
        printf("Pilih lagu yang ingin diputar: ");
        scanf("%s", command);
        PlaySong(command);
      }
      else if (strcmp(buf.mtext, "LIST")==0){
        showPlaylist();
      }
      else if (strcmp(buf.mtext, "ADD")==0){
        Add();
      }
      else printf("UNKNOWN COMMAND\n");

  }
  strcpy(buf.mtext, "end");
  lenght = strlenght(buf.mtext);
   if (msgsnd(msqid, &buf, lenght+1, 0) == -1) /* +1 for '\0' */
  perror("msgsnd");

  if (msgctl(msqid, IPC_RMID, NULL) == -1) {
      perror("msgctl");
      exit(1);
  }
  printf("Message queue: Pesan telah berhasil disampaikan.\n");
  return 0;
}