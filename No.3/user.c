#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>

#define PERMISSION 0666
struct messageBuffer {
   long mtype;
   char mtext[200];
};

int main(void) {
   struct messageBuffer buf;
   int msqid;
   int toend;
   key_t key;
   
   if ((key = ftok("song_playlist.txt", 'B')) == -1) {
      perror("ftok");
      exit(1);
   }
   
   if ((msqid = msgget(key, PERMISSION)) == -1) {
      perror("Message queue tidak ditemukan");
      exit(1);
   }
   printf("Message queue: Siap menerima message .\n");
   
   for(;;) {
      if (msgrcv(msqid, &buf, sizeof(buf.mtext), 0, 0) == -1) {
         perror("msgrcv");
         exit(1);
      } else {
        if(strcmp(buf.mtext,"DECRYPT")==0){
          printf("recvd: \"%s\"\n", buf.mtext);
            toend = strcmp(buf.mtext,"end");
            if (toend == 0)
            break;
        }
      }
   }
   printf("message queue: done receiving messages.\n");
   system("rm song_playlist.txt");
   return 0;
}